using HeatPump
using JLD
using XLSX
using Plots
using StatsPlots

fls = load("C:/Users/mlfasci/.julia/dev/HeatPump/FLS_50y_granite_r0575_H100.jld")["fls"];
flsN = load("C:/Users/mlfasci/.julia/dev/HeatPump/neighbours_influence_25y_granite_d15_r165_H100.jld")["flsN"]
Tair = XLSX.readxlsx("C:/Users/mlfasci/Box Sync/PhD (mlfasci@kth.se 4)/Project/Conferences/WGC 2020/T_Stockholm.xlsx")["Sheet1"]["H3:H8762"] .+ 273.15;

Tcondin = load("C:/Users/mlfasci/.julia/dev/HeatPump/example/House_Output_tr")["tr"];
loadhp = load("C:/Users/mlfasci/.julia/dev/HeatPump/example/House_Output_Load")["hourload_No_DHW"];

Tcondin_DHW = fill(50. + 273.15, 8760);
loadhp_DHW = load("C:/Users/mlfasci/.julia/dev/HeatPump/example/DHW_load.jld")["DHW_load"];


n = 25;  # number of years
data = HP_Data();
parameters = HP_Parameters();

brine_flowrate_eva = 0.3;
water_flowrate_cond = 0.4;
H = 100.;
Rb = 0.15;


trial_ground_neighbourhood = hp_ground(n, HP_Parameters(), HP_Data(), brine_flowrate_eva, water_flowrate_cond, fls, flsN, H, Rb, Tcondin, loadhp, Tcondin_DHW, loadhp_DHW);
trial_dual_neighbourhood = dualheatpump(n, parameters, data, brine_flowrate_eva, water_flowrate_cond, fls, flsN, H, Rb, Tcondin_DHW, loadhp_DHW, Tcondin, loadhp, Tair);



ontime_allowed = fill(1., n*8760);
solution_air_DHW = air_heatpump(parameters, data, brine_flowrate_eva, water_flowrate_cond, Tair, Tcondin_DHW, loadhp_DHW, ontime_allowed);
ontime_allowed_air =  1 .-solution_air_DHW[6]
solution_air = air_heatpump(parameters, data, brine_flowrate_eva, water_flowrate_cond, Tair, Tcondin, loadhp, ontime_allowed_air);

# save("HouseH1002016_DHW_corrected.jld","solution_air", solution_air,"trial_ground", trial_ground,"trial_dual", trial_dual,"solution_air_DHW", solution_air_DHW)
save("NeighbourhoodH1002016_B15_DHW_corrected.jld","solution_air", solution_air,"solution_air_DHW", solution_air_DHW,"trial_ground_neighbourhood", trial_ground_neighbourhood,"trial_dual_neighbourhood", trial_dual_neighbourhood)


ontime_allowed_ground = 1 .-solution_ground_DHW[6]
ontime_allowed_dual = 1 .-solution_dual_DHW[6]
solution_air_DHW = air_heatpump(parameters, data, brine_flowrate_eva, water_flowrate_cond, Tair, Tcondin_DHW, loadhp_DHW, ontime_allowed);
solution_air = air_heatpump(parameters, data, brine_flowrate_eva, water_flowrate_cond, Tair, Tcondin, loadhp, ontime_allowed_air);
solution_ground = isolated_system(n, parameters, data, brine_flowrate_eva, water_flowrate_cond, fls, H, Rb, Tcondin, loadhp, ontime_allowed_ground);
solution_dual = dualheatpump(n, parameters, data, brine_flowrate_eva, water_flowrate_cond, fls, H, Rb, Tcondin, loadhp, Tair, ontime_allowed_dual);

save("HouseH502016_DHW.jld","solution_air", solution_air,"solution_ground", solution_ground,"solution_dual", solution_dual,"solution_air_DHW", solution_air_DHW,"solution_ground_DHW", solution_ground_DHW,"solution_dual_DHW", solution_dual_DHW)

###

res = load("C:/Users/mlfasci/.julia/dev/HeatPump/example/NeighbourhoodH1002016_B20_DHW_corrected.jld");
####
solution_air = res["solution_air"];
solution_ground = res["trial_ground_neighbourhood"];
solution_dual = res["trial_dual_neighbourhood"];

solution_air_DHW = res["solution_air_DHW"];
# solution_ground_DHW = res["solution_ground_DHW"];
# solution_dual_DHW = res["solution_dual_DHW"];

##############
el_air_yearly = fill(0.,25,1);
el_ground_yearly = fill(0.,25,1);
el_dual_yearly = fill(0.,25,1);

el_air_yearly_DHW = fill(0.,25,1);
el_ground_yearly_DHW = fill(0.,25,1);
el_dual_yearly_DHW = fill(0.,25,1);

for ii = 1:25
    el_air_yearly_DHW[ii] = sum(solution_air_DHW[8] + solution_air_DHW[9]);
    el_air_yearly[ii] = sum(solution_air[8] + solution_air[9]);
    el_ground_yearly_DHW[ii] = sum(solution_ground[14][(ii-1)*8760+1 : ii*8760] + solution_ground[15][(ii-1)*8760+1 : ii*8760]);
    el_ground_yearly[ii] = sum(solution_ground[8][(ii-1)*8760+1 : ii*8760] + solution_ground[9][(ii-1)*8760+1 : ii*8760]);
    el_dual_yearly_DHW[ii] = sum(solution_dual[15][(ii-1)*8760+1 : ii*8760] + solution_dual[16][(ii-1)*8760+1 : ii*8760]);
    el_dual_yearly[ii] = sum(solution_dual[8][(ii-1)*8760+1 : ii*8760] + solution_dual[9][(ii-1)*8760+1 : ii*8760]);
end
el_yearly_tot = hcat(el_air_yearly[vcat(1,25)] .+ el_air_yearly_DHW[vcat(1,25)], el_ground_yearly[vcat(1,25)] .+ el_ground_yearly_DHW[vcat(1,25)], el_dual_yearly[vcat(1,25)] .+ el_dual_yearly_DHW[vcat(1,25)])
el_yearly = hcat(el_air_yearly[vcat(1,25)], el_ground_yearly[vcat(1,25)], el_dual_yearly[vcat(1,25)])

el_ground_yearly_DHW[25] + el_ground_yearly[25]
el_dual_yearly_DHW[25] + el_dual_yearly[25]

(el_yearly_tot[2,1] - el_yearly_tot[2,2]) / el_yearly_tot[2,1]
(el_yearly_tot[2,1] - el_yearly_tot[2,3]) / el_yearly_tot[2,1]
# groupedbar(el_yearly_tot, bar_position = :dodge, bar_width=0.5, ylim = [0, 8e6])
# groupedbar!(el_yearly, bar_position = :dodge, bar_width=0.5)
# #############
# savefig("NeighbourhoodH100d202016_DHW.svg")

###
##
investment_cost_ground = hp_cost(8) + drilling_cost(50);
investment_cost_air = airhp_cost(8);

power_air = (el_air_yearly_DHW .+ el_air_yearly) ./ 1e3; # kWh
power_ground = (el_ground_yearly_DHW .+ el_ground_yearly) ./ 1e3; # kWh
power_dual = (el_dual_yearly_DHW .+ el_dual_yearly) ./ 1e3; # kWh
heat = repeat([sum(loadhp .+loadhp_DHW)], n) ./ 1e3; #kWh
# lcoh_ground = lcoh(investment_cost_ground, power_ground, heat)
# npv_ground = npv(investment_cost_ground, power_ground, heat)

el_price = collect(1:0.2:3)
npv_ground = fill(0., length(el_price))
npv_air = fill(0., length(el_price))
npv_groundm20 = fill(0., length(el_price))
npv_airm20 = fill(0., length(el_price))
npv_groundp20 = fill(0., length(el_price))
npv_airp20 = fill(0., length(el_price))


levcost_ground = fill(0., length(el_price))
levcost_air = fill(0., length(el_price))

keyword = "null"

for ee = 1 : length(el_price)
    npv_ground[ee] = npv(investment_cost_ground , power_ground, heat; electricity_price = el_price[ee])
    npv_air[ee] = npv(investment_cost_air , power_air, heat; electricity_price = el_price[ee])
    npv_groundm20[ee] = npv(investment_cost_ground * 0.8 , power_ground, heat; electricity_price = el_price[ee])
    npv_airm20[ee] = npv(investment_cost_air * 0.8, power_air, heat; electricity_price = el_price[ee])
    npv_groundp20[ee] = npv(investment_cost_ground * 1.2 , power_ground, heat; electricity_price = el_price[ee])
    npv_airp20[ee] = npv(investment_cost_air * 1.2, power_air, heat; electricity_price = el_price[ee])

    # levcost_ground[ee] = lcoh(investment_cost_ground, power_ground, heat, electricity_price = el_price[ee])
    # levcost_air[ee] = lcoh(investment_cost_air, power_air, heat, electricity_price = el_price[ee])

    if npv_ground[ee] > npv_air[ee]
        keyword = "ground"
    else
        keyword = "air"
    end

    # levcost_ground[ee] = lcoh(investment_cost_ground, power_ground, heat, electricity_price = el_price[ee])
    # levcost_air[ee] = lcoh(investment_cost_air, power_air, heat, electricity_price = el_price[ee])
    if keyword == "ground"
        max_cost[ee] =  max_hex_cost(power_ground, power_dual, electricity_price = el_price[ee])
    else
        max_cost[ee] =  max_hex_cost(power_air, power_dual, electricity_price = el_price[ee]) -  (investment_cost_ground - investment_cost_air)
    end
end

scatter(el_price, max_cost, ylim = [-3e4, 2e4])
savefig("N50_20.svg")

plot(el_price, npv_groundm20, fillrange = npv_groundp20, ylim = [0, 5e5])
plot!(el_price, npv_airm20, fillrange = npv_airp20)
scatter!(el_price, npv_air)
scatter!(el_price, npv_ground)


savefig("NPVN50_20.svg")

plot(el_price, levcost_air)
plot!(el_price, levcost_ground)


Tair = XLSX.readxlsx("C:/Users/mlfasci/Box Sync/PhD (mlfasci@kth.se 4)/Project/Conferences/WGC 2020/T_Stockholm.xlsx")["Sheet1"]["H3:H8762"];

T_brine_ground = solution_ground[13][1:8760] .- 273.15;
T_brine_dual = solution_dual[12][1:8760] .- 273.15;

T_brine_ground = solution_ground[13][24*8760+1:25*8760] .- 273.15;
T_brine_dual = solution_dual[12][24*8760+1:25*8760] .- 273.15;

plot(Tair[:,1])
plot!(T_brine_ground)
plot!(T_brine_dual)
# groupedbar(rand(10,3), bar_position = :dodge, bar_width=0.7)
# # histogram([sum(solution_dual[8])], bins = 1)
# # histogram!([sum(solution_dual[9])], bins = 1)
#
#
# scatter( solution_dual[end], markershape = :o, color = :red, ylim = [0,1])
# plot!(twinx(),Tair .- 273.15, color = :blue, ylim = [- 10, 30], size=(600, 400))
#
# plot(solution_ground[11])
# plot(solution_ground[8])  # Energy consumed by the compressor
#
# El_tot_ground = solution_ground[8] .+ solution_ground[9];
#
# sum(El_tot_ground)


H = 25.;
t = collect(3600. : 3600 : 3600. * 8760 * 25 );
k = 3.1;
δr = 0.0575;
D = 6.
αg = k/(870*2300);
response = fls(t, αg, δr, H, H,D, D) ./ (2*pi*k);

save("FLS_25y_granite_r0575_H25.jld", "fls", response)



Tf -Rb- Tbh



## creating new flsN files
using Grids
using SFLS_validation
r = 165.;
B = 15.;
D = 6.;
H = 100.;
m = 1;
Psrc = cylinder(B, r, D, H, m)


field_characteristics = fill(0., length(Psrc), 5)

for ii = 1: length(Psrc)
    field_characteristics[ii,1] = Psrc[ii][1];
    field_characteristics[ii,2] = Psrc[ii][2];
    field_characteristics[ii,3] = D;
    field_characteristics[ii,4] = H;
    field_characteristics[ii,5] = 1; # [W/m]
end
rb = 0.0575
k = 3.1;
αg = k/ (2300*870);
tmax = 25;
res = FLS_I(field_characteristics, rb, αg, tmax) ./ (2*pi*k)
pos = findmax(res[1])[2][1]

field_characteristics = fill(0., length(Psrc), 5)
for ii = 1: length(Psrc)
    field_characteristics[ii,1] = Psrc[ii][1];
    field_characteristics[ii,2] = Psrc[ii][2];
    field_characteristics[ii,3] = D;
    field_characteristics[ii,4] = H;
    field_characteristics[ii,5] = 0.; # [W/m]
end
field_characteristics[pos,5] = 1.;
fls_self = FLS_I(field_characteristics, rb, αg, tmax)[1] ./ (2*pi*k)
flsN = res[1][pos,:] .- fls_self[pos,:]


save("neighbours_influence_25y_granite_d15_r165_H100.jld", "flsN", flsN)

flsNold = load("C:/Users/mlfasci/.julia/dev/HeatPump/neighbours_influence_25y_granite_d15_r165_H100.jld")["flsN"]
##
