using HeatPump
using JLD
using XLSX


path = string(dirname(pwd()), "\\HeatPump\\example\\files\\T_Stockholm.xlsx")
Tair = XLSX.readxlsx(path)["Sheet1"]["H3:H8762"] .+ 273.15;

path = string(dirname(pwd()), "\\HeatPump\\example\\files\\House_Output_tr")
Tcondin = load(path)["tr"];
path = string(dirname(pwd()), "\\HeatPump\\example\\files\\House_Output_Load")
loadhp = load(path)["hourload_No_DHW"];

Tcondin_DHW = fill(50. + 273.15, 8760);
path = string(dirname(pwd()), "\\HeatPump\\example\\files\\DHW_load.jld")
loadhp_DHW = load(path)["DHW_load"];


solution_air = airheatpump(Tair, Tcondin, loadhp, Tcondin_DHW, loadhp_DHW);

el_air_yearly = fill(0.,n,1);
el_air_yearly_DHW = fill(0.,n,1);
for ii = 1:n
    el_air_yearly_DHW[ii] = sum(solution_air[3][(ii-1)*8760+1 : ii*8760] + solution_air[4][(ii-1)*8760+1 : ii*8760]);
    el_air_yearly[ii] = sum(solution_air[1][(ii-1)*8760+1 : ii*8760] + solution_air[2][(ii-1)*8760+1 : ii*8760]);
end
