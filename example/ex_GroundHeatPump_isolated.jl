using HeatPump
using JLD
using XLSX


path = string(dirname(pwd()), "\\HeatPump\\example\\files\\House_Output_tr")
Tcondin = load(path)["tr"];
path = string(dirname(pwd()), "\\HeatPump\\example\\files\\House_Output_Load")
loadhp = load(path)["hourload_No_DHW"];

Tcondin_DHW = fill(50. + 273.15, 8760);
path = string(dirname(pwd()), "\\HeatPump\\example\\files\\DHW_load.jld")
loadhp_DHW = load(path)["DHW_load"];

path = string(dirname(pwd()), "\\HeatPump\\example\\files\\FLS_50y_granite_r0575_H100.jld")
fls = load(path)["fls"];

n = 1;  # number of years
H = 100.;

solution_ground = groundheatpump(n, fls, H, Tcondin, loadhp, Tcondin_DHW, loadhp_DHW);

el_ground_yearly = fill(0.,n,1);
el_ground_yearly_DHW = fill(0.,n,1);
for ii = 1:n
    el_ground_yearly_DHW[ii] = sum(solution_ground[3][(ii-1)*8760+1 : ii*8760] + solution_ground[4][(ii-1)*8760+1 : ii*8760]);
    el_ground_yearly[ii] = sum(solution_ground[1][(ii-1)*8760+1 : ii*8760] + solution_ground[2][(ii-1)*8760+1 : ii*8760]);
end
