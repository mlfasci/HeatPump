using HeatPump
using Test
using CSV
using DataFrames
using JLD
using GLM

include("test_FindParameters.jl")
include("test_regression.jl")
