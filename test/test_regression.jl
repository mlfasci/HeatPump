"""
    generate_catalogue(hp_data::HP_Data, hp_parameters::HP_Parameters, file_name::String; Brine::String = "INCOMP::MEA-28%", Teva_in_low::T = -12., Teva_in_up::T = 10., Tcond_in_low::T = 20., Tcond_in_up::T = 60., brine_flowrate_eva_low::T = 0.1, brine_flowrate_eva_up::T = 0.4, water_flowrate_cond_low::T = 0.2, water_flowrate_cond_up::T = 0.4) where{T<:AbstractFloat}
"""
# file_name = "test_generate_catalogue_new.csv"
# @time computed = generate_catalogue(HP_Data(), HP_Parameters(), file_name, Teva_in_low = 2., Teva_in_up = 3., Tcond_in_low = 40., Tcond_in_up = 42., brine_flowrate_eva_low = 0.2, brine_flowrate_eva_up = 0.3, water_flowrate_cond_low = 0.2, water_flowrate_cond_up = 0.3)
# df = DataFrame(CSV.File("test_generate_catalogue.csv"))
#
# @test df == computed

"""
    generate_regressions(df_file::String)
"""
file_name = joinpath(pwd(),"test_generate_catalogue.csv")
regrHC, regrCOP = generate_regressions(file_name)
coefficientsHC = coef(regrHC)
coefficientsCOP = coef(regrCOP)
correct_regr = load("test_coefficients_regrHC_COP.jld")
correct_regrHC = correct_regr["coefficientsHC"]
correct_regrCOP = correct_regr["coefficientsCOP"]
#
@test coefficientsHC == correct_regrHC
@test coefficientsCOP == correct_regrCOP

"""
    predict_performance(regrHC, regrCOP,v_s::T, v_l::T, t_is::T, t_il::T) where{T<:AbstractFloat}
"""
v_s = 0.1;
v_l = 0.2
t_is = -2.;
t_il = 40.;
res = predict_performance(regrHC, regrCOP,v_s, v_l, t_is, t_il)

res1_correct = 5.759930991554898
res2_correct = 3.101941507042043

@test res[1][1] ≈ res1_correct
@test res[2][1] ≈ res2_correct
