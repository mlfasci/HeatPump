# hp_parameters = [2.32, 0.001194, 8.79285395552048e-6, 1.00000, 0.771033328549176, 4.830478921019937e-11, 1149, 6725];
# brine_flowrate_eva = 0.3;
# water_flowrate_cond = 0.4;
# Teva_inx = 265.;
# Tcond_inx = 340.;
# hp_capacity(Teva_inx, Tcond_inx, brine_flowrate_eva, water_flowrate_cond, HP_Parameters(), HP_Data())
"""
    hp_capacity(Teva_inx::T, Tcond_inx::T, brine_flowrate_eva::T, water_flowrate_cond::T, hp_parameters::HP_Parameters, hp_data::HP_Data) where {T<:AbstractFloat}
        Teva_inx: brine temperature at the inlet of the evaporator [K];
        Tcond_inx: water temperature of the water at the inlet of the condenser [K];

        hp_parameters: vector containing the 8 parameters needed to simulate the HP;

    Returns the heating capacity [W] and cooling capacity [W] of the heat  pump at the given conditions (Teva_inx, Tcond_inx).

    For more info read: "Cimmino, M., Wetter, M., 2017. Modelling of Heat Pumps with Calibrated Parameters Based on Manufacturer Data. Presented at the The 12th International Modelica Conference, Prague, Czech Republic, May 15-17, 2017, pp. 219–226. https://doi.org/10.3384/ecp17132219"
"""
function hp_capacity(Teva_inx::T, Tcond_inx::T, brine_flowrate_eva::T, water_flowrate_cond::T, hp_parameters::HP_Parameters, hp_data::HP_Data; Brine::String = "INCOMP::MEA-25%") where {T<:AbstractFloat}

    @unpack Teva_in, Tcond_in, CompressorPower, HeatingCapacity, Refrigerant = hp_data;

    @unpack Vr, V, C, ΔTsuperheating, compressor_efficiency, W_loss, UAeva, UAcond = hp_parameters;

    p_eva_brine = 101325.;                                                      # [Pa]
    p_cond_water = 101325.;                                                     # [Pa]

    # Determination of the guess values for the heat pump capacity, cooling and heating capacity based on the performance map.
        pos_eva = findall(x->x==Teva_in[findmin(abs.(Teva_in .- Teva_inx))[2]], Teva_in);
        pos_cond = findall(x->x==Tcond_in[findmin(abs.(Tcond_in .- Tcond_inx))[2]], Tcond_in);
        pos = intersect(pos_eva, pos_cond);
        Qcond_guess = HeatingCapacity[pos[1]];
        Wcomp_guess = CompressorPower[pos[1]];
        Qeva_guess = Qcond_guess - Wcomp_guess;
    #---------------------------------------------------------------------------
    # Tcond, Teva = repeat([0.],2);

    maximum_error = 0.01;
    Error_guess_HC = 2 * maximum_error;
    Error_guess_CP = 2 * maximum_error;
    while Error_guess_HC > maximum_error  || Error_guess_CP > maximum_error
        cp_brine = CoolProp.PropsSI("C", "P", p_eva_brine, "T", Teva_inx, Brine);                              # specific heat of the secondary fluid in the evaporator [J/kgK]
        cp_water = CoolProp.PropsSI("C", "P", p_cond_water, "T", Tcond_inx , "water");                         # specific heat of the water in the condenser [J/kgK]

        density_brine = CoolProp.PropsSI("D", "P", p_eva_brine, "T", Teva_inx, Brine);                              # specific heat of the secondary fluid in the evaporator [J/kgK]
        density_water = CoolProp.PropsSI("D", "P", p_cond_water, "T", Tcond_inx , "water");                         # specific heat of the water in the condenser [J/kgK]

        efficiency_eva = 1 - exp( -UAeva / (brine_flowrate_eva * density_brine *  cp_brine));                                                  # effectiveness of the evaporator [-]
        efficiency_cond = 1 - exp(-UAcond / (water_flowrate_cond * density_water * cp_water));                                                 # effectiveness of the condenser [-]

        Teva = Teva_inx - Qeva_guess / (efficiency_eva *cp_brine * brine_flowrate_eva * density_brine );                                  # Temperature of the refrigerant in the evaporator [K]
        Tcond = Tcond_inx + Qcond_guess / (efficiency_cond * cp_water * water_flowrate_cond * density_water);                             # Temperature of the refrigerant in the condenser [K]
        Tsuction = Teva + ΔTsuperheating;                                                                                              # Temperature of the refrigerant at the inlet of the compressor [K]

        p_eva = CoolProp.PropsSI("P", "T", Teva, "Q", 1.0, Refrigerant);
        p_cond = CoolProp.PropsSI("P", "T", Tcond, "Q", 1.0, Refrigerant);

        hcondenser_out = CoolProp.PropsSI("H", "T", Tcond, "Q", 0.0, Refrigerant);
        hsuction = CoolProp.PropsSI("H", "T", Tsuction, "P", p_eva, Refrigerant);
        hevaporator_out = CoolProp.PropsSI("H", "T", Teva, "Q", 1.0, Refrigerant);

        density = CoolProp.PropsSI("D", "T", Tsuction, "P", p_eva, Refrigerant);                                            # density of the refrigerant [kg/m^3] (the suction pressure is assumed queal to the evaporator pressure!)
        m_leak = C * (p_cond / p_eva);                                                                                   # mass flow rate leaked [kg/s]

        m_ref = V * density - m_leak;                                                                                   # mass flow rate of refrigerant [kg/s]
        cp = CoolProp.PropsSI("CPMASS", "T", Tsuction, "P", p_eva, Refrigerant);
        cv = CoolProp.PropsSI("CVMASS", "T", Tsuction, "P", p_eva, Refrigerant);

        gamma = cp/cv;                                                                                                       # isentropic coefficient [-]
        pr = Vr ^ gamma;                                                                                                   # pressure ratio [-]
        Wt = gamma / (gamma - 1) * p_eva * V * (((gamma - 1 ) / gamma) * (p_cond / (p_eva * Vr)) + (1 / gamma) * pr^ ((gamma - 1) / gamma) - 1);    # Mechanical compressor power [W]

        W = Wt / compressor_efficiency + W_loss;                                                                                                 # Compressor power [W]
        Qeva = m_ref * (hevaporator_out - hcondenser_out);                                                                    # Cooling Capacity [W]
        Qcond = W + Qeva;                                                                                                     # Heating Capacity [W]

        Error_guess_HC = abs(Qcond - Qcond_guess) / Qcond_guess;                                                               # relative error on the heating capacity
        Error_guess_CP = abs(W -  Wcomp_guess) /  Wcomp_guess;                                                                 # relative error on the heating capacity

        Qcond_guess = Qcond;
        Qeva_guess = Qeva;
        Wcomp_guess = W;
    end
    Qcond = Qcond_guess;
    Qeva = Qeva_guess;
    Tcond = Tcond;
    Teva = Teva;

    return [Qcond, Qeva, Tcond, Teva];
end #hp_capacity(Teva_inx::T, Tcond_inx::T, brine_flowrate_eva::T, water_flowrate_cond::T, hp_parameters, hp_data::HP_Data) where {T<:AbstractFloat}
 """
    hp_onoff(Qcond::T, Qeva::T, W::T, Qload::T) where {T <:AbstractFloat}
        Qcond: heating capacity [W]
        Qeva:  cooling capacity [W]
        W:     compressor power [W]

        Qload: heating load [W]

  Returns: 1. the share of the hour when the HP was turned on, ontime; 2. The energy extracted from the heat source, extraction; 3. The electricity consumed by the compressor, compressorenergy, 4. The electricity required by the auxiliary heating, Q_aux.
"""
function hp_onoff(Qcond::T, Qeva::T, W::T, Qload::T, ontime_allowed::T) where {T <:AbstractFloat}
    ontime = Qload / Qcond;                      # share of the time when the HP is on [-]
    if ontime <= ontime_allowed
        extraction = Qeva * ontime;
        compressorenergy = W * ontime;
        Q_aux = 0.;
    else                                         # the house requires auxiliary heating
        Q_aux = Qload - Qcond * ontime_allowed;
        extraction = Qeva * ontime_allowed;
        compressorenergy = W * ontime_allowed;
        ontime = ontime_allowed;
    end
    return [ontime, extraction, compressorenergy, Q_aux]
end
