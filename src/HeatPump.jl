module HeatPump
    include("FindParameters.jl")
        export HP_Data, HP_Parameters
        export nominalparameters
        export parameters_cost
        export optimizedparameters
        export parameters_accuracy
    include("HP_Operation.jl")
        export hp_capacity
        export hp_onoff
    include("regression.jl")
        export generate_catalogue
        export generate_regressions
        export predict_performance
    include("AirHeatPump.jl")
        export air_heatpump
    include("DualHeatPump.jl")
        export dualheatpump
    include("GroundHeatPump.jl")
        export groundheatpump
        export groundheatpump_LA

    using CoolProp
    using CSV
    using DataFrames
    using Distributed
    using GLM
    using Optim
    using Parameters
    using LoadAggregation

  function self_dT(q::Array{T,1}, fls::Array{T,1})::T where {T<:AbstractFloat}
         nsteps = length(q);
         sum = 0.;
         for kk=1: nsteps-1
                 sum = sum + q[kk] * (fls[(nsteps) - kk] - fls[(nsteps) - kk + 1]);
         end
         dT = -sum + q[end] * fls[1];
         return dT
  end

end # module
