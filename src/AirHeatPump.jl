"""
    air_heatpump(Tair, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4) where {T <:AbstractFloat}

Simulate the operation of an air-source heat pump during 1 year.

# Output:

1. `EcompressorSH` Electricity used by the compressor in space heating mode [Wh]
2. `EauxSH` Energy used by the auxiliary in space heating mode [Wh]
3. `EcompressorDHW` Electricity used by the compressor in space heating mode [Wh]
4. `EauxDHW` Electricity used by the auxiliary in domestic hot water mode [Wh]
5. `ontimeSH` share of the hour during which the HP operates in space heating mode [-]
6. `ontimeDHW` share of the hour during which the HP operates domestic hot water [-]
7. `auxonflagSH` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in space heating mode [-]
8. `auxonflagDHW` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in domestic hot water mode [-]
9. `QcondSH` heating capacity in space heating mode [W]
10. `QevaSH` cooling capacity in space heating mode [W]
11. `QcondDHW` heating capacity domestic hot water [W]
12. `QevaDHW` cooling capacity  domestic hot water [W]
13. `TcondSH` condensation temperature in space heating mode [K]
14. `TevaSH` evaporation temperature in space heating mode[K]
15. `TcondDHW` condensation temperature domestic hot water [K]
16. `TevaDHW` evaporation temperature domestic hot water [K]

# Arguments:

1. `Tair` Temperature of the ambient air [K]
4. `TcondinSH` Temperature of the return temperature from the space heating distribution system [K]
5. `loadSH` Heat required for space heating [Wh]
6. `TcondinDHW` Temperature of the return temperature from the domestic hot water production distribution system [K]
7. `loadDHW` Heat required for hot water production [Wh]

## Keyword arguments:

- `parameters::HP_Parameters = HP_Parameters()` Heat Pump thermodynamical parameters as in "" [-]
- `data::HP_Data = HP_Data()` Heat Pump manufacturer data as in "" [-]
- `Teva_min = -12 + 273.15` minimum return brine temperature below which the HP is turned off and the auxiliary is used instead
- `brine_flowrate_eva = 0.3` Mass flow rate of the secondary fluid in the borehole heat exchanger [kg/s]
- `water_flowrate_cond = 0.4` Mass flow rate of the water in the heating distribution system [kg/s]
"""
  function air_heatpump(Tair, Tcond_in_SH::Array{T,1}, QloadSH, Tcond_in_DHW::Array{T,1}, QloadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), Teva_min::T = -12+273.15, brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4) where {T <:AbstractFloat}
      @unpack Brine = data;

    QevaSH = fill(0.0, 8760);                                                            # HP cooling capacity in space heating mode [W],
    TevaSH = fill(0.0, 8760);                                                            # Refrigerant evaporation T in space heating mode [K]
    EevaSH = fill(0.0, 8760);                                                            # Energy absorbed in the evaporator in space heating mode [Wh]
    QevaDHW = fill(0.0, 8760);                                                           # HP cooling capacity in domestic hot water mode [W],
    TevaDHW = fill(0.0, 8760);                                                           # Refrigerant evaporation T in domestic hot water mode [K]
    EevaDHW = fill(0.0, 8760);                                                           # Energy absorbed in the evaporator in domestic hot water mode [Wh]

    Qcond_nc = fill(0.0, 8760);                                                             # HP Heating capacity in space heating mode [W]
    QcondSH = fill(0.0, 8760);                                                             # HP Heating capacity in space heating mode [W]
    TcondSH = fill(0.0, 8760);                                                             # Refrigerant evaporation T in space heating mode [K]
    EcondSH = fill(0.0, 8760);                                                             # Energy realeased in the condenser in space heating mode [Wh]
    QcondDHW = fill(0.0, 8760);                                                             # HP Heating capacity in domestic hot water mode [W]
    TcondDHW = fill(0.0, 8760);                                                             # Refrigerant evaporation T in domestic hot water mode [K]
    EcondDHW = fill(0.0, 8760);                                                             # Energy realeased in the condenser in domestic hot water mode [Wh]

    EauxSH = fill(0.0, 8760);                                                           # Energy for space heating delivered to the house by the auxiliary system [Wh]
    EauxDHW = fill(0.0, 8760);                                                          # Energy for domestic hot water delivered to the house by the auxiliary system [Wh]

    WcompressorSH = fill(0.0, 8760);                                                        # Compressor power in space heating mode [W]
    WcompressorDHW = fill(0.0, 8760);                                                       # Compressor power in domestic hot water mode [W]
    EcompressorSH = fill(0.0, 8760);                                                       # Energy used by the compressor for space heating [Wh]
    EcompressorDHW = fill(0.0, 8760);                                                      # Energy used by the compressor for domestic hot water [Wh]

    ontimeSH = fill(0.0, 8760);                                                             # Share of the hour while the HP is on for space heating [-]
    ontimeDHW = fill(0.0, 8760);                                                            # Share of the hour while the HP is on for domestic hot water[-]

    auxonflagSH = fill(0.0, 8760);                                                         # Flag indicating if the HP is used for spaced heating
    auxonflagDHW = fill(0.0, 8760);                                                        # Flag indicating if the HP is used for domestic hot water
  ## end initialization
for z = 1: 8760
  if Tair[z] - 3 < Teva_min   # if the ground is too cold run the auxiliary system
      auxonflagSH[z] = 2;           # Only the auxiliary is used
      auxonflagDHW[z] = 2;           # Only the auxiliary is used
      EauxDHW[z] =  QloadDHW[z];  # all the domestic hot water demand is satisfied by the auxiliary
      EauxSH[z] = QloadSH[z];     # all the space heating demand is satisfied by the auxiliary
  else
      HPperformance = hp_capacity(Tair[z] -3, Tcond_in_DHW[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
      TcondDHW[z], TevaDHW[z] = HPperformance[3], HPperformance[4];
      Qcond_nc[z], QevaDHW[z] = HPperformance[1], HPperformance[2];
      WcompressorDHW[z] = Qcond_nc[z] - QevaDHW[z];
      if Tair[z] < 7 + 273.15
          if Tair[z] >  273.15
              F = min(1, 0.816 + 0.0177 * (Tair[z] - 273.15));
          else
              F = 0.816;
          end
          QcondDHW[z] = F * Qcond_nc[z];
      else
          QcondDHW[z] = Qcond_nc[z]
      end

      HPdelivered = hp_onoff(QcondDHW[z], QevaDHW[z], WcompressorDHW[z], QloadDHW[z], 1.);
      ontimeDHW[z] = HPdelivered[1];
      EevaDHW[z], EcompressorDHW[z], EauxDHW[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];

      EauxDHW[z] > 0.0 ? auxonflagDHW[z] = 1 : auxonflagDHW[z] = 0;

      # SH
      HPperformance = hp_capacity(Tair[z] -3, Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
      TcondSH[z], TevaSH[z] = HPperformance[3], HPperformance[4];
      Qcond_nc[z], QevaSH[z] = HPperformance[1], HPperformance[2];
      WcompressorSH[z] = Qcond_nc[z] - QevaSH[z];
      if Tair[z] < 7 + 273.15
          if Tair[z] >  273.15
              F = min(1, 0.816 + 0.0177 * (Tair[z] - 273.15));
          else
              F = 0.816;
          end
          QcondSH[z] = F * Qcond_nc[z];
      else
          QcondSH[z] = Qcond_nc[z]
      end

      HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1. - ontimeDHW[z]);
      ontimeSH[z] = HPdelivered[1];
      EevaSH[z], EcompressorSH[z], EauxSH[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];

      EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;

    end
end

    return [EcompressorSH, EauxSH, EcompressorDHW, EauxDHW, ontimeSH, ontimeDHW, auxonflagSH, auxonflagDHW, QcondSH, QevaSH, QcondDHW, QevaDHW, TcondSH, TevaSH, TcondDHW, TevaDHW];
    end
