using Parameters

@with_kw struct HP_Data
# # DATA for the HP ETON6kW (Heating Mode). From: ETON.pdf, page 12/52
# #
    Brine = "water";
    # Brine = "INCOMP::MEA-25%"
    Refrigerant = "R410A";

    #Performance map--------------------------------------------------------
        CompressorPower = [1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.8, 1.8, 1.8, 1.8, 1.8, 1.8, 2.0, 2.0, 2.0, 2.0, 2.0, 2.1 ].* 1e3;   # [W]
        CoolingCapacity = [5.4, 5.5, 5.7, 5.9, 6.1, 6.3, 5.1, 5.3, 5.5, 5.6, 5.8, 6.0, 4.9, 5.0, 5.2, 5.3, 5.5, 5.7, 4.6, 4.7, 4.9, 5.0, 5.2, 5.3].* 1e3;    # [W]

        HeatingCapacity = CompressorPower + CoolingCapacity;                                                                                                 # [W]

        Teva_in = [5., 6., 7., 8., 9., 10., 5., 6., 7., 8., 9., 10., 5., 6., 7., 8., 9., 10., 5., 6., 7., 8., 9., 10.].+ 273.;                               # Brine inlet temperature at the evaporator [K]
        Teva_out =  Teva_in .- 5.;                                                                                                                           # Brine outlet temperature from the evaporator [K]
        Tcond_in = [30., 30., 30., 30., 30., 30., 35, 35, 35, 35, 35, 35, 40, 40, 40, 40, 40, 40, 45, 45, 45, 45, 45, 45].+ 273.;                             # Water inlet temperatrue at the condenser [K]
        Tcond_out = Tcond_in .+ 5.;                                                                                                                           # Water outlet temperature from the condenser [K]
#     #-----------------------------------------------------------------------

## DATA for the HP NIBE8kW
# -> Notice that points 9-12 were excluded for giving bad results. These points were also experimentally obtained differently points 1-8.
    #
    # Brine = "INCOMP::MEA-25%"; # 25% ethanol
    # Refrigerant = "R407C";
    #
    # #Performance map complete (wrong)--------------------------------------------------------
    #  #    CompressorPower = [1.64,1.83,1.94,2.01,1.82,2.08,2.27,2.44,0.91,1.04,1.13,1.22].* 1e3;                 # Compressor power [W]
    #  #    HeatingCapacity = [7.67, 6.70, 5.93, 4.96, 10.73, 9.70, 8.87, 7.77, 5.37, 4.85, 4.43, 3.88].* 1e3;    # Heating Capacity [W]                                                                                        # Heating capacity [W]
    #  #
    #  #    CoolingCapacity = HeatingCapacity .- CompressorPower;
    #  #
    #  #    Teva_in = [0., 0., 0., 0., 10., 10., 10., 10., 15., 15., 15., 15.].+ 273.;                                  # Brine inlet temperature at the evaporator [K]
    #  #    Teva_out = [-3., -3., -3., -3., 7., 7., 7., 7., 14.55, 8.45, 13.50 , 13.50].+ 273.;                        # Brine outlet temperature from the evaporator [K]
    #  # ### Details about Teva_out after the performance map ###
    #  #    Tcond_in = [30., 40., 47., 55., 30., 40., 47., 55., 30., 40., 47., 55.].+ 273.;                             # Water inlet temperatrue at the condenser [K]                                                                                      # Water inlet temperature at the condenser [K]
    #  #    Tcond_out = [35., 45., 55., 65., 35., 45., 55., 65., 35., 45., 55., 65.].+ 273.;                            # Water outlet temperature from the condenser [K]
    #
    # #Performance map corrected--------------------------------------------------------
    #     CompressorPower = [1.64, 1.83, 1.94, 2.01, 1.82, 2.08, 2.27, 2.44].* 1e3;                 # Compressor power [W]
    #     HeatingCapacity = [7.67, 6.70, 5.93, 4.96, 10.73, 9.7, 8.87, 7.77].* 1e3;    # Heating Capacity [W]                                                                                        # Heating capacity [W]
    #
    #     CoolingCapacity = HeatingCapacity .- CompressorPower;
    #
    #     Teva_in = [0., 0., 0., 0., 10., 10., 10., 10].+ 273.;                                  # Brine inlet temperature at the evaporator [K]
    #     Teva_out = [-3., -3., -3., -3., 7., 7., 7., 7.].+ 273.;                        # Brine outlet temperature from the evaporator [K]
    #     Tcond_in = [30., 40., 47., 55., 30., 40., 47., 55.].+ 273.;                             # Water inlet temperatrue at the condenser [K]                                                                                      # Water inlet temperature at the condenser [K]
    #     Tcond_out = [35., 45., 55., 65., 35., 45., 55., 65.].+ 273.;
    #

end

#-----------------------------------------------------------------------
# ## Teva_out is not fixed for points 9-12. Instead the brine flow rate is fixed equal to the brine flow rate 5-8.
# ## Therefore Teva_out was calculated as follows:
#
#         #Step 1
#         cp = fill(0., 4);
#         brine_flowrate_eva = fill(0., 4);
#         for i=1:4
#             cp[i] = CoolProp.PropsSI("C", "P", 101325., "T", (Teva_in[i+4] + Teva_out[i+4])/2, Brine);
#             brine_flowrate_eva[i] = (HeatingCapacity[i+4] - CompressorPower[i+4])/ ((Teva_in[i+4] - Teva_out[i+4]) * cp[i]);
#         end
#
#         # =>brine_flowrate_eva = [3.3441578145832898, 0.24425809578486926, 0.6728766014455794, 0.5779737744208849];
#
#         #Step 2
#         cp = fill(0., 4);
#         x = fill(0., 4); # x->Teva_out
#         brine_flowrate_eva = [3.3441578145832898, 0.24425809578486926, 0.6728766014455794, 0.5779737744208849];
#         for i=1:4
#             cp[i] = CoolProp.PropsSI("C", "P", 101325., "T", Teva_in[i+8], Brine);
#             x[i] = Teva_in[i+8] - (HeatingCapacity[i+8] - CompressorPower[i+8]) / brine_flowrate_eva[i] / cp[i];
#         end

################################################################################
