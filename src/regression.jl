using GLM
using CSV
"""
    generate_catalogue(hp_data::HP_Data, hp_parameters::HP_Parameters; Brine::String = "INCOMP::MEA-28%", Teva_in_low::T = -12., Teva_in_up::T = 10., Tcond_in_low::T = 20., Tcond_in_up::T = 60., brine_flowrate_eva_low::T = 0.1, brine_flowrate_eva_up::T = 0.4, water_flowrate_cond_low::T = 0.2, water_flowrate_cond_up::T = 0.5) where{T<:AbstractFloat}

Create the heat pump data in the format used as an input in the regression model in [1] and implemented in the function `generate_regressions`. It uses the grey-box model described in [2] to generate the data.

    [1] Simon, F., Ordoñez, J., Reddy, T.A., Girard, A., Muneer, T., 2016.
        Developing multiple regression models from the manufacturer’s ground-source heat pump catalogue data. Renewable Energy 95, 413–421.
        https://doi.org/10.1016/j.renene.2016.04.045
    [2] Cimmino, M., Wetter, M., 2017.
        Modelling of Heat Pumps with Calibrated Parameters Based on Manufacturer Data.
        Presented at the The 12th International Modelica Conference, Prague, Czech Republic, May 15-17, 2017, pp. 219–226.
        https://doi.org/10.3384/ecp17132219

# Output:
1. `df`: dataframe containing the generated data
2. CSV file named after `file_name` containing the dataframe witht the generated data

# Arguments:
1. `hp_data`
2. `hp_parameters`
3. `file_name`

## Keyword arguments (optional):
 - `Brine = "INCOMP::MEA-28%"`
 - `Teva_in_low = -12.`
 - `Teva_in_up = 10.`
 - `Tcond_in_low = 20.`
 - `Tcond_in_up = 60.`
 - `brine_flowrate_eva_low = 0.1`
 - `brine_flowrate_eva_up = 0.4`
 - `water_flowrate_cond_low = 0.2`
 - `water_flowrate_cond_up::T = 0.4`

"""
function generate_catalogue(hp_data::HP_Data, hp_parameters::HP_Parameters, file_name::String; Brine::String = "INCOMP::MEA-28%", Teva_in_low::T = -12., Teva_in_up::T = 10., Tcond_in_low::T = 20., Tcond_in_up::T = 60., brine_flowrate_eva_low::T = 0.1, brine_flowrate_eva_up::T = 0.4, water_flowrate_cond_low::T = 0.2, water_flowrate_cond_up::T = 0.4) where{T<:AbstractFloat}
    Teva_in = collect(Teva_in_low : 1 : Teva_in_up) .+ 273.15
    Tcond_in = collect(Tcond_in_low : 2 : Tcond_in_up) .+ 273.15
    brine_flowrate_eva = collect(brine_flowrate_eva_low : 0.05 : brine_flowrate_eva_up)
    water_flowrate_cond = collect(water_flowrate_cond_low : 0.05 : water_flowrate_cond_up)

    HC = fill(0., length(Teva_in) * length(Tcond_in) * length(brine_flowrate_eva) * length(water_flowrate_cond))
    COP = fill(0., length(Teva_in) * length(Tcond_in) * length(brine_flowrate_eva) * length(water_flowrate_cond))

    df = DataFrame(v_s2 = [], t_is_v_s = [], t_is_v_l = [],	v_s_v_l = [], t_il_v_l = [], t_il2 = [], t_is_t_il = [], v_s_t_il = [],	t_is = [], v_s = [], t_il = [],	v_l = [], HC = [], COP = []);
    for ii = 1 : length(Teva_in)
        for jj = 1 : length(Tcond_in)
            for kk = 1 : length(brine_flowrate_eva)
                for ll = 1 : length(water_flowrate_cond)
                    res = hp_capacity(Teva_in[ii], Tcond_in[jj], brine_flowrate_eva[kk]/1e3, water_flowrate_cond[ll]/1e3, hp_parameters, hp_data, Brine = Brine)
                    push!(df, [brine_flowrate_eva[kk]^2 (Teva_in[ii]-273.15)*brine_flowrate_eva[kk] (Teva_in[ii]- 273.15)*water_flowrate_cond[ll] brine_flowrate_eva[kk]*water_flowrate_cond[ll] (Tcond_in[jj] - 273.15)*water_flowrate_cond[ll] (Tcond_in[jj] - 273.15)^2 (Teva_in[ii]- 273.15)*(Tcond_in[jj]- 273.15) brine_flowrate_eva[kk] * (Tcond_in[jj] - 273.15) (Teva_in[ii]- 273.15) brine_flowrate_eva[kk] (Tcond_in[jj] - 273.15) water_flowrate_cond[ll] res[1]/1000 res[1]/(res[1] - res[2])])
                end
            end
        end
    end
    CSV.write(file_name, df)
    return df
end
"""
    generate_regressions(df_file::String)

Create the regression models for the Heating Capcity and COP of a HP given the manufacturer data.

# Output:

1. `regrHC`: regression model for the heating capacity
2. `regrCOP`: regression model for the COP

# Arguments:
1. `df_file`: path to the file containing the dataframe with HP data

The file must be formatting in the following way:

    v_s2, t_is_v_s, t_is_v_l , v_s_v_l , t_il_v_l , t_il2 , t_is_t_il , v_s_t_il, t_is, v_s, t_il, v_l, HC, COP
"""
function generate_regressions(df_file::String)
    df = DataFrame(CSV.File(df_file))

    regrHC = lm(@formula(HC ~ v_s2 + t_is_v_s + t_is_v_l + v_s_v_l + t_il_v_l + t_is + t_il + v_l ), df)
    regrCOP = lm(@formula(COP ~ t_il2 + t_is_v_s + t_is_t_il + t_is_v_l + v_s_t_il + t_il_v_l + t_il + v_l), df)

    return regrHC, regrCOP
end
"""
    predict_performance(regrHC, regrCOP,v_s, v_l, t_is, t_il)

Calculate the heating capacity and COP of a HP from its regression models.

# Output:
1. `HC`: heating capacity [kW]
2. `COP`: COP [-]

# Arguments:
1. `regrHC`: regression model to calculate the heating capcity
2. `COP`: regression model to calculate the COP
3. `v_s`: volumetric flow rate of the secondary fluid in the evaporator [l/s]
4. `v_l`: volumetric flow rate of the secondary fluid in the condenser [l/s]
5. `t_is`: temperature of the secondary fluid at the inlet of the evaporator [C]
6. `t_il`: temperature of the secondary fluid at the inlet of the condenser [C]
"""
function predict_performance(regrHC, regrCOP,v_s::T, v_l::T, t_is::T, t_il::T) where{T<:AbstractFloat}
    dfHC = DataFrame(v_s2 = v_s^2, t_is_v_s = t_is*v_s, t_is_v_l = t_is * v_l, v_s_v_l = v_s*v_l, t_il_v_l = t_il*v_l, t_is = t_is, t_il = t_il, v_l = v_l)
    HC = predict(regrHC, dfHC)
    dfCOP = DataFrame(t_il2 = t_il^2, t_is_v_s = t_is * v_s, t_is_t_il = t_is * t_il, t_is_v_l = t_is * v_l, v_s_t_il = v_s * t_il, t_il_v_l = t_il * v_l, t_il = t_il, v_l = v_l)
    COP = predict(regrCOP, dfCOP)

    return HC, COP
end
