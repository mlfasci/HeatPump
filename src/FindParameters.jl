include("struct_HP_Data.jl")
include("struct_HP_Parameters.jl")
"""
    nominalparameters(nominaldata::HP_Data, ii::Int64)
    nominalparameters(nominaldata::HP_Data*, ΔTsuperheating::T, compressor_efficiency::T, ΔTevaporator::T, ΔTcondenser::T, compressor_leakage_coefficient::T, ii::Int) where {T<:AbstractFloat}
        ii: point of the performance map to use to find the nominalparameters. (When nominal data are not given any point of the performance map can be used instead)
        *HP_Data is the structure defined in "struct_HP_Data.jl". Some data is offered as default. To consider different HPs, change this data in the structure.

    Calculates the parameters of a HP that match the nominal data (or another point in the performance map) of the HP. These values can be used as guess values to evaluate the parameters that best fit the whole catalogue data.
    For more info read: "Cimmino, M., Wetter, M., 2017. Modelling of Heat Pumps with Calibrated Parameters Based on Manufacturer Data. Presented at the The 12th International Modelica Conference, Prague, Czech Republic, May 15-17, 2017, pp. 219–226. https://doi.org/10.3384/ecp17132219"
"""
function nominalparameters(data::HP_Data, ii::Int64)
    ΔTsuperheating = 4.;                                                    # [K]
    compressor_efficiency = 0.95;                                           # [-]
    ΔTevaporator = 4.;                                                      # temperature difference between the evaporating refrigerant and the brine at the inlet of the evaporator [K]
    ΔTcondenser = 4.;                                                       # temperature difference between the condensing refrigerant and the water at the inlet of the condenser [K]
    compressor_leakage_coefficient = 0.01;                                  # [-]

    nominal_parameters = nominalparameters(data, ΔTsuperheating, compressor_efficiency, ΔTevaporator, ΔTcondenser, compressor_leakage_coefficient, ii);
    return nominal_parameters
end # nominalparameters(data::HP_Data, ii::Int64)
function nominalparameters(data::HP_Data, ΔTsuperheating::T, compressor_efficiency::T, ΔTevaporator::T, ΔTcondenser::T, compressor_leakage_coefficient::T, ii::Int64) where {T<:AbstractFloat}
    @unpack Refrigerant, Brine, Teva_in, Tcond_in, Teva_out, Tcond_out, CompressorPower, HeatingCapacity, CoolingCapacity = data;  # extract the required data

    Teva_ref = Teva_in[ii] - ΔTevaporator;                                                  # Temperature of the refrigerant in the evaporator [K]
    Tcond_ref = Tcond_in[ii] + ΔTcondenser;                                                 # Temperature of the refrigerant in the condenser [K]
    p_eva = CoolProp.PropsSI("P", "T", Teva_ref, "Q", 1.0, Refrigerant);                    # refrigerant pressure in the evaporator, calculated assuming saturated vapor conditions [Pa]
    p_cond = CoolProp.PropsSI("P", "T", Tcond_ref, "Q", 0.0, Refrigerant);                  # refrigerant pressure in the condenser, calculated assuming saturated liquid conditions [Pa]

    Tsuction_ref = Teva_ref + ΔTsuperheating;                                               # temperature of the refrigerant at the inlet of the compressor [K]
    hsuction = CoolProp.PropsSI("H", "T", Tsuction_ref, "P", p_eva, Refrigerant);           # enthalpy of the refrigerant at the inlet of the compressor (superheated vapor) [J/kg]
    density = CoolProp.PropsSI("D", "T", Tsuction_ref, "P", p_eva, Refrigerant);                # density of the refrigerant at the inlet of the compressor [kg/m3]
    Cp = CoolProp.PropsSI("CPMASS", "T", Tsuction_ref, "P", p_eva, Refrigerant);                # specific heat at constant pressure at the inlet of the compressor [J/kg/K]
    Cv = CoolProp.PropsSI("CVMASS", "T", Tsuction_ref, "P", p_eva, Refrigerant);                # specific heat at constant volume at the inlet of the compressor [J/kg/K]
    gamma = Cp / Cv;                                                                        # isentropic coefficient of the refrigerant at the inlet of the compressor [-]

    Vr = (p_cond / p_eva) ^ (1 / gamma);                                                                   # compressor volume ratio [-]

    heva_out = CoolProp.PropsSI("H", "P", p_eva, "Q", 1.0, Refrigerant);                     # enthalpy of the refrigerant at the outlet of the evaporator (saturated vapor) [J/kg]
    hcond_out = CoolProp.PropsSI("H", "P", p_cond, "Q", 0.0, Refrigerant);                   # enthalpy of the refrigerant at the outlet of the condenser (saturated liquid) [J/kg] = enthalpy of the refrigerant at the inlet of the evaporator (lamination from the condesor outlet) [J/kg]
    m_ref = CoolingCapacity[ii] / (heva_out - hcond_out);                                    # refrigerant mass flow rate [kg/s]

    V = m_ref * (1 + compressor_leakage_coefficient) / density;                             # refrigerant volume flow rate [m3/s]
    C = (m_ref * compressor_leakage_coefficient) / (p_cond/p_eva);                          # leakage coefficient of the compressor [kg/s]
    Wt = gamma / (gamma-1) * p_eva * V * ( ( (gamma-1) / gamma) * (p_cond / (p_eva * Vr)) + (1 / gamma) * (p_cond/p_eva) ^ ( (gamma-1) / gamma ) -1);            # theoretical scroll compressor work [W]
    W_loss = max(0, CompressorPower[ii] - Wt / compressor_efficiency);                      # Constant part of power losses [W], different from Cimmino (check the incoherence in Cimmino)

    # LMTDeva = (Teva_in[ii] - Teva_out[ii])/(log(Teva_in[ii] - Teva_ref) - log(Teva_out[ii]-Teva_ref));
    # LMTDcond = -(Tcond_in[ii] - Tcond_out[ii])/ log((Tcond_in[ii]-Tcond_ref)/(Tcond_out[ii]-Tcond_ref));
    # UAeva = CoolingCapacity[ii] / LMTDeva;
    # UAcond = HeatingCapacity[ii] / LMTDcond;
    UAeva = CoolingCapacity[ii] / ΔTevaporator ;
    UAcond = HeatingCapacity[ii] / ΔTcondenser;

    nominal_parameters = [Vr, V, C, ΔTsuperheating, compressor_efficiency, W_loss, UAeva, UAcond];
    return nominal_parameters
end # nominalparameters(data::HP_Data, ΔTsuperheating::T, compressor_efficiency::T, ΔTevaporator::T, ΔTcondenser::T, compressor_leakage_coefficient::T, ii::Int64) where {T<:AbstractFloat}
"""
    parameters_cost(Teva_in::T, Teva_out::T, Tcond_in::T, Tcond_out::T, CompressorPower::T, HeatingCapacity::T, CoolingCapacity::T, p_eva_brine::T, p_cond_water::T, x, data::HP_Data) where {T<:AbstractFloat}
        Teva_in: Temperature of the brine at the inlet of the evaporator [K]
        Teva_out: Temperature of the brine at the outlet of the evaporator [K]
        Tcond_in: Temperature of the water at the inlet of the condenser [K]
        Tcond_out: Temperature of the water at the outlet of the condenser [K]
        x: vector containing the parameters of the HP ([Vr, V, C, ΔTsuperheating, compressor_efficiency, W_loss, UAeva, UAcond])
    Returns:
    1. The cost (error) of estimating the data (CompressorPower, HeatingCapacity) with the use of parameters and thermodynamical equations;
    2. the relative error between the given heating capacity and calculated heating capacity;
    3. the relative error between the given compressor power and calculated compressor power;
    4. Cooling capacity;
    5. Heating capacity;
    6. Evaporation temperature (refrigerant);
    7. Condensation temperature (refrigerant);

    For more info read: "Cimmino, M., Wetter, M., 2017. Modelling of Heat Pumps with Calibrated Parameters Based on Manufacturer Data. Presented at the The 12th International Modelica Conference, Prague, Czech Republic, May 15-17, 2017, pp. 219–226. https://doi.org/10.3384/ecp17132219"
"""
function parameters_cost(Teva_in::T, Teva_out::T, Tcond_in::T, Tcond_out::T, CompressorPower::T, HeatingCapacity::T, CoolingCapacity::T, p_eva_brine::T, p_cond_water::T, x, data::HP_Data) where {T<:AbstractFloat}
    @unpack Brine, Refrigerant = data;

    cp_brine = CoolProp.PropsSI("C", "P", p_eva_brine, "T", (Teva_in + Teva_out) / 2, Brine);                              # specific heat of the secondary fluid in the evaporator [J/kgK]
    cp_water = CoolProp.PropsSI("C", "P", p_cond_water, "T", (Tcond_in + Tcond_out) / 2, "water");                         # specific heat of the water in the condenser [J/kgK]

    brine_flowrate_eva = CoolingCapacity / ((Teva_in - Teva_out) * cp_brine);                                            # [kg/s]
    water_flowrate_cond = HeatingCapacity / ((Tcond_out - Tcond_in) * cp_water);                                          # [kg/s]

    efficiency_eva = 1 - exp( -x[7] / (brine_flowrate_eva * cp_brine));                                                  # effectiveness of the evaporator [-]
    efficiency_cond = 1 - exp(-x[8] / (water_flowrate_cond * cp_water));                                                 # effectiveness of the condenser [-]

    Teva = Teva_in - CoolingCapacity / (efficiency_eva *cp_brine * brine_flowrate_eva);                                  # Temperature of the refrigerant in the evaporator [K]
    Tcond = Tcond_in + HeatingCapacity / (efficiency_cond * cp_water * water_flowrate_cond);                             # Temperature of the refrigerant in the condenser [K]
    Tsuction = Teva + x[4];                                                                                              # Temperature of the refrigerant at the inlet of the compressor [K]

    p_eva = CoolProp.PropsSI("P", "T", Teva, "Q", 1.0, Refrigerant);
    p_cond = CoolProp.PropsSI("P", "T", Tcond, "Q", 0.0, Refrigerant);

    hcondenser_out = CoolProp.PropsSI("H", "T", Tcond, "Q", 0.0, Refrigerant);
    hsuction = CoolProp.PropsSI("H", "T", Tsuction, "P", p_eva, Refrigerant);
    hevaporator_out = CoolProp.PropsSI("H", "T", Teva, "Q", 1.0, Refrigerant);

    density = CoolProp.PropsSI("D", "T", Tsuction, "P", p_eva, Refrigerant);                                            # density of the refrigerant [kg/m^3] (the suction pressure is assumed queal to the evaporator pressure!)
    m_leak = x[3] * (p_cond / p_eva);                                                                                   # mass flow rate leaked [kg/s]

    m_ref = x[2] * density - m_leak;                                                                                   # mass flow rate of refrigerant [kg/s]
    cp = CoolProp.PropsSI("CPMASS", "T", Tsuction, "P", p_eva, Refrigerant);
    cv = CoolProp.PropsSI("CVMASS", "T", Tsuction, "P", p_eva, Refrigerant);

    gamma = cp/cv;                                                                                                       # isentropic coefficient [-]
    pr = x[1] ^ gamma;                                                                                                   # pressure ratio [-]
    Wt = gamma / (gamma - 1) * p_eva * x[2] * (((gamma - 1 ) / gamma) * (p_cond / (p_eva * x[1])) + (1 / gamma) * pr^ ((gamma - 1) / gamma) - 1);    # Mechanical compressor power [W]

    W = Wt / x[5] + x[6];                                                                                                 # Compressor power [W]
    Qeva = m_ref * (hevaporator_out - hcondenser_out);                                                                    # Cooling Capacity [W]
    Qcond = W + Qeva;                                                                                                     # Heating Capacity [W]

    Error_guess_HC = abs(Qcond - HeatingCapacity) / HeatingCapacity;                                                               # relative error on the heating capacity
    Error_guess_CP = abs(W -  CompressorPower) /  CompressorPower;                                                                 # relative error on the heating capacity

    return [((W - CompressorPower) / CompressorPower)^2 + ((Qcond - HeatingCapacity) / HeatingCapacity)^2, Error_guess_HC, Error_guess_CP, Qeva, Qcond, Teva, Tcond];
end # parameters_cost(Teva_in::T, Teva_out::T, Tcond_in::T, Tcond_out::T, CompressorPower::T, HeatingCapacity::T, CoolingCapacity::T, p_eva_brine::T, p_cond_water::T, x, data::HP_Data) where {T<:AbstractFloat}

"""
   optimizedparameters(data::HP_Data)
   optimizedparameters(data::HP_Data, p_eva_brine::T, p_cond_water::T) where {T<:AbstractFloat}

   Returns the optimized parameters to model an HP. These are the parameters to be stored and used for the HP simulation. WARNING: the solution depends on lower and upper bounds set inside the function!

   For more info read: "Cimmino, M., Wetter, M., 2017. Modelling of Heat Pumps with Calibrated Parameters Based on Manufacturer Data. Presented at the The 12th International Modelica Conference, Prague, Czech Republic, May 15-17, 2017, pp. 219–226. https://doi.org/10.3384/ecp17132219"

   BE CAREFUL! The parameters depend on the lower and upper boundaries set within this function!!! You might have to change them!!
 """
 function optimizedparameters(data::HP_Data)
   p_eva_brine = 101325.;                                # pressure of the secondary fluid in the evaporator [Pa]
   p_cond_water = 101325.;                               # pressure of water in the condenser [Pa]
   return OptimizedParameters = optimizedparameters(data, p_eva_brine, p_cond_water)
 end
 function optimizedparameters(data::HP_Data, p_eva_brine::T, p_cond_water::T) where {T<:AbstractFloat}
   @unpack Brine, Refrigerant = data;                                                                               # extract the required data
   @unpack Teva_in, Tcond_in, Teva_out, Tcond_out, CompressorPower, HeatingCapacity, CoolingCapacity = data;        # extract the required data

   Nparameters = 8;                                                                                                 # number of parameters used to model the HP
   measuredpoints = length(CompressorPower);
   nominal_parameters = zeros(Nparameters, measuredpoints)                                                          # nominal parameters are calucalted for each point in the performanace map!

   for ii = 1 : measuredpoints                                                                     # calculate and store in a matrix the parameters that perfectly fit each point of the performance map
       nominal_parameters[:, ii] = nominalparameters(data, ii);
   end

   lower_bound = zeros(Nparameters,1);                                                             # initialize the lower bound of the perameters to ise in the optimization routine
   upper_bound = zeros(Nparameters,1);                                                             # initialize the upper bound of the perameters to ise in the optimization routine
   #### Setting lower and upper bounds is tricky and might require manual adjustments for each case. Order of the parameters: [Vr, V, C, ΔTsuperheating, compressor_efficiency, W_loss, UAeva, UAcond]
       lower_bound[1] = minimum(nominal_parameters[1,:])/20;
       lower_bound[2] = minimum(nominal_parameters[2,:])/20;
       lower_bound[3] = minimum(nominal_parameters[3,:])/20;
       lower_bound[4] = 0.1;
       lower_bound[5] = 0.;
       lower_bound[6] = 0;
       lower_bound[7] = minimum(nominal_parameters[7,:])/20;
       lower_bound[8] = minimum(nominal_parameters[8,:])/20;

       upper_bound[1] = max(5., maximum(nominal_parameters[1,:]));
       upper_bound[2] = maximum(nominal_parameters[2,:])*20;
       upper_bound[3] = maximum(nominal_parameters[3,:])*20;
       upper_bound[4] = 6.;
       upper_bound[5] = 0.99;
       upper_bound[6] = maximum(CompressorPower[1:measuredpoints]) /3;
       upper_bound[7] = maximum(nominal_parameters[7,:])*20;
       upper_bound[8] = maximum(nominal_parameters[8,:])*20;
     ##########################################################################

   guess_parameters = zeros(Nparameters,1);
   guess_parameters[:] = nominal_parameters[:, div(measuredpoints,2)+1];       # use the central point of the performance map as initial guess for the parameters

   ObjectiveFunction(x) = sum(parameters_cost(Teva_in[i], Teva_out[i], Tcond_in[i], Tcond_out[i], CompressorPower[i], HeatingCapacity[i], CoolingCapacity[i], p_eva_brine, p_cond_water, x, data)[1] for i in 1:measuredpoints)
   OptimizedParameters = optimize(ObjectiveFunction,lower_bound, upper_bound, guess_parameters);

   return OptimizedParameters, guess_parameters, lower_bound, upper_bound
 end
 """
    parameters_accuracy(parameters::HP_Parameters, data::HP_Data)
    parameters_accuracy(parameters::HP_Parameters, data::HP_Data, p_eva_brine::T, p_cond_water::T) where {T<:AbstractFloat}

    Uses the thermodynamical parameters to calculate the Heating Capacity and Compressor Power in the conditions tested by the manufacturer.
    Compares the calculated values with the values measured during the tests. Returns the relative error for both the heating capacity and compressor power.
  """
  function parameters_accuracy(parameters::HP_Parameters, data::HP_Data)
        return parameters_accuracy(HP_Parameters(), HP_Data(), 101325., 101325.)
  end # function parameters_accuracy(parameters::HP_Parameters, data::HP_Data)
  function parameters_accuracy(parameters::HP_Parameters, data::HP_Data, p_eva_brine::T, p_cond_water::T) where {T<:AbstractFloat}
      @unpack Teva_in, Teva_out, Tcond_in, Tcond_out, CompressorPower, HeatingCapacity, CoolingCapacity = data;              # extract the required data
      @unpack Brine, Refrigerant, Vr, V, C, ΔTsuperheating, compressor_efficiency, W_loss, UAeva, UAcond = parameters;       # extract the required data

      x = [Vr, V, C, ΔTsuperheating, compressor_efficiency, W_loss, UAeva, UAcond];
      measuredpoints = length(CompressorPower);
      Error_HC = zeros(measuredpoints,1);
      Error_Power = zeros(measuredpoints,1);
      for ii = 1 : measuredpoints
        Results = parameters_cost(Teva_in[ii], Teva_out[ii], Tcond_in[ii], Tcond_out[ii], CompressorPower[ii], HeatingCapacity[ii], CoolingCapacity[ii], p_eva_brine, p_cond_water, x, data)
        Error_HC[ii] = Results[2];
        Error_Power[ii] = Results[3];
      end
      return Error_HC, Error_Power
  end # function parameters_accuracy(parameters::HP_Parameters, data::HP_Data, p_eva_brine::T, p_cond_water::T) where {T<:AbstractFloat}
