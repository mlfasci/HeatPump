function drilling_cost(H)
    C0 = 9300; #[SEK]
    C1 = 158.53; # [SEK/ m]
    C2 = 3.38e-4; # [SEK/ m^3]
    C3 = 100; # [SEK/ m]

    nb = 1;

    inflationrate = 0.02;
    Nyears = 1; # Constants related to 2018
    P = ((C1 + C2 * H^2 + C3) * nb * H + C0)

    for ii = 1: Nyears
        P = P * (1+inflationrate*(ii-1));
    end
    return P * 1.25
end

function hp_cost(heating_capacity)
    # P = 2198.8 * heating_capacity + 75920;
     P = 2690.8 * heating_capacity + 61854;
end

function npv(investment_cost, power, heat::Array{T,1}; lifetime = 25, electricity_price::T=1.5, discount_rate::T= 0.03) where{T<:AbstractFloat}
    net_present_value = - investment_cost;
    for yy = 1 : lifetime
        net_present_value = net_present_value + ((heat[yy] - power[yy]) * electricity_price/(1+discount_rate)^yy);
    end
    return net_present_value
end
