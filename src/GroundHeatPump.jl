"""
    groundheatpump(n::Int, fls::Array{T,1}, H::T, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva::T = 0.3, water_flowrate_cond::T = 0.4, Rb::T = 0.15) where {T <:AbstractFloat}

Simulate the operation of a ground-source heat pump used for space heating and/or domestic hot water during `n` years neglecting the presence of neighbouring installations

# Output:

1. `EcompressorSH` electricity used by the compressor in space heating mode [Wh]
2. `EauxSH` energy used by the auxiliary in space heating mode [Wh]
3. `EcompressorDHW` electricity used by the compressor in domestic hot water mode [Wh]
4. `EauxDHW` electricity used by the auxiliary in domestic hot water mode [Wh]
5. `ontimeSH` share of the hour during which the HP operates in space heating mode [-]
6. `ontimeDHW` share of the hour during which the HP operates in domestic hot water mode [-]
7. `auxonflagSH` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in space heating mode [-]
8. `auxonflagDHW` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in domestic hot water mode [-]
9. `QcondSH` heating capacity in space heating mode [W]
10. `QevaSH` cooling capacity in space heating mode [W]
11. `QcondDHW` heating capacity domestic hot water mode [W]
12. `QevaDHW` cooling capacity  domestic hot water mode [W]
13. `TcondSH` condensation temperature in space heating mode [K]
14. `TevaSH` evaporation temperature in space heating mode [K]
15. `TcondDHW` condensation temperature in domestic hot water mode [K]
16. `TevaDHW` evaporation temperature in domestic hot water mode [K]
17. `EgroundSH .+ EgroundDHW` heat absorbed from the ground [Wh]
18. `T_bh` borehole wall temperature [K]
19. `Teva_in_hp` return temperature from the ground heat exchnager [K]

# Arguments:

1. `n` number of years to simulate [-]
2. `fls` thermal response of the borehole to its own operation. Must already be scaled by 1/(2* pi *k) [m*K/W]
3. `H` borehole length [m]
4. `TcondinSH` temperature of the return temperature from the space heating distribution system [K]
5. `loadSH` heat required for space heating [Wh]
6. `TcondinDHW` temperature of the return temperature from the domestic hot water production distribution system [K]
7. `loadDHW` heat required for hot water production [Wh]

## Keyword arguments:

- `Rb` borehole resistance [m*K/W]
- `parameters` heat hump thermodynamical parameters as in 'struct_HP_Parameters.jl' [-]
- `data` heat hump manufacturer data as in 'struct_HP_Data.jl' [-]
- `brine_flowrate_eva` mass flow rate of the secondary fluid in the borehole heat exchanger [kg/s]
- `water_flowrate_cond ` mass flow rate of the water in the heating distribution system [kg/s]
"""
function groundheatpump(n::Int, fls::Array{T,1}, H::T, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4, Rb::T = 0.15) where {T <:AbstractFloat}
    # @unpack Brine = data;
    Brine = "INCOMP::MEA-30%"

## Initializing the variables
    T_bh = fill(0.0, 8760 * n);                                                             # Borehole wall temperature [K]
    # This should be changed sothat the user can input their own value
    T_bh[1] = 8. + 273.;                                                                    # The initial borehole temperature is 8 ºC
    #
    Teva_in_hp = fill(0.0, 8760 * n);                                                       # Temperature of the fluid circulating in the borehole
    Teva_in_hp[1] = T_bh[1] - 3;                                                            # Initial return temperature of fluid circulating in the borehole 3 K lower than the borehole temperature. The best would be to find it iterating.

    Tcond_in_SH = repeat(TcondinSH, n);                                                     # Return temperature from the distribution system for space heating (water temperature at the condenser inlet) [K]
    Tcond_in_DHW = repeat(TcondinDHW, n);                                                   # Return temperature from the distribution system for domestic hot water (water temperature at the condenser inlet) [K]
    QloadSH = repeat(loadSH, n);                                                            # Heating demand for space heating [Wh]
    QloadDHW = repeat(loadDHW, n);                                                          # Heating demand for domestic hot water [Wh]

    QevaSH = fill(0.0, 8760 *n);                                                            # HP cooling capacity in space heating mode [W],
    TevaSH = fill(0.0, 8760 *n);                                                            # Refrigerant evaporation T in space heating mode [K]
    EevaSH = fill(0.0, 8760 *n);                                                            # Energy absorbed in the evaporator in space heating mode [Wh]
    EgroundSH = fill(0.0, 8760 *n);                                                         # Energy extracted from the BH in space heating mode [Wh]
    QevaDHW = fill(0.0, 8760 *n);                                                           # HP cooling capacity in domestic hot water mode [W],
    TevaDHW = fill(0.0, 8760 *n);                                                           # Refrigerant evaporation T in domestic hot water mode [K]
    EevaDHW = fill(0.0, 8760 *n);                                                           # Energy absorbed in the evaporator in domestic hot water mode [Wh]
    EgroundDHW = fill(0.0, 8760 *n);                                                        # Energy extracted from the BH in domestic hot water mode [Wh]

    QcondSH = fill(0.0, 8760 *n);                                                           # HP Heating capacity in space heating mode [W]
    TcondSH = fill(0.0, 8760 *n);                                                           # Refrigerant evaporation T in space heating mode [K]
    EcondSH = fill(0.0, 8760 *n);                                                           # Energy realeased in the condenser in space heating mode [Wh]
    QcondDHW = fill(0.0, 8760 *n);                                                          # HP Heating capacity in domestic hot water mode [W]
    TcondDHW = fill(0.0, 8760 *n);                                                          # Refrigerant evaporation T in domestic hot water mode [K]
    EcondDHW = fill(0.0, 8760 *n);                                                          # Energy realeased in the condenser in domestic hot water mode [Wh]

    EauxSH = fill(0.0, 8760 * n);                                                           # Energy for space heating delivered to the house by the auxiliary system [Wh]
    EauxDHW = fill(0.0, 8760 * n);                                                          # Energy for domestic hot water delivered to the house by the auxiliary system [Wh]

    WcompressorSH = fill(0.0, 8760 *n);                                                     # Compressor power in space heating mode [W]
    WcompressorDHW = fill(0.0, 8760 *n);                                                    # Compressor power in domestic hot water mode [W]
    EcompressorSH = fill(0.0, 8760 * n);                                                    # Energy used by the compressor for space heating [Wh]
    EcompressorDHW = fill(0.0, 8760 * n);                                                   # Energy used by the compressor for domestic hot water [Wh]

    ontimeSH = fill(0.0, 8760 *n);                                                          # Share of the hour while the HP is on for space heating [-]
    ontimeDHW = fill(0.0, 8760 *n);                                                         # Share of the hour while the HP is on for domestic hot water[-]

    auxonflagSH = fill(0.0, 8760 * n);                                                      # Flag indicating if the HP is used for spaced heating
    auxonflagDHW = fill(0.0, 8760 * n);                                                     # Flag indicating if the HP is used for domestic hot water
## end initialization
    for z = 1 : n * 8760
        if Teva_in_hp[z] < -12 + 273.15   # if the ground is too cold run the auxiliary system. The threshold temperature should be initialized by the user
            if z < n * 8760
                AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls); # Temperature chang on the borehole wall [K]
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
            auxonflagSH[z] = 2;           # Only the auxiliary is used
            auxonflagDHW[z] = 2;          # Only the auxiliary is used
            EauxDHW[z] =  QloadDHW[z];  # all the domestic hot water demand is satisfied by the auxiliary
            EauxSH[z] = QloadSH[z];     # all the space heating demand is satisfied by the auxiliary
        else # use the HP
        # Domestic Hot water
            HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_DHW[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
            TcondDHW[z], TevaDHW[z] = HPperformance[3], HPperformance[4];
            QcondDHW[z], QevaDHW[z] = HPperformance[1], HPperformance[2];
            WcompressorDHW[z] = QcondDHW[z] - QevaDHW[z];

            HPdelivered = hp_onoff(QcondDHW[z], QevaDHW[z], WcompressorDHW[z], QloadDHW[z], 1.);
            ontimeDHW[z] = HPdelivered[1];
            EevaDHW[z], EcompressorDHW[z], EauxDHW[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];
            EgroundDHW[z] = HPdelivered[2];

            EauxDHW[z] > 0.0 ? auxonflagDHW[z] = 1 : auxonflagDHW[z] = 0;
            EcondDHW[z] = QcondDHW[z] * ontimeDHW[z];

            AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls);
            T_bh[z] = T_bh[1] - AT_bh;
            Teva_in_hp[z] = T_bh[z] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
        # Space Heating
            if Teva_in_hp[z] < -12 + 273.15   # if the ground is too cold run the auxiliary system
                if z < n * 8760
                    T_bh[z+1] = T_bh[z];
                    Teva_in_hp[z+1] = Teva_in_hp[z];
                end
                auxonflagSH[z] = 2;   # All the space heating demand is satisfied by the auxiliary
                EauxSH[z] = QloadSH[z];
            else
                HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
                TcondSH[z], TevaSH[z] = HPperformance[3], HPperformance[4];
                QcondSH[z], QevaSH[z] = HPperformance[1], HPperformance[2];
                WcompressorSH[z] = QcondSH[z] - QevaSH[z];

                HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1. - ontimeDHW[z]);

                EgroundSH[z] = HPdelivered[2];
                EevaSH[z] = HPdelivered[2];
                EcompressorSH[z] = HPdelivered[3];
                EauxSH[z] = HPdelivered[4];

                EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;
                EcondSH[z] = QcondSH[z] * HPdelivered[1]
                ontimeSH[z] = HPdelivered[1];

                EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;

                if z < n * 8760
                    AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls);
                    T_bh[z+1] = T_bh[1] - AT_bh;
                    Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] + EgroundDHW[z]) / H * Rb + (EgroundSH[z] + EgroundDHW[z])  / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
                end
            end
        end
    end
return [EcompressorSH, EauxSH, EcompressorDHW, EauxDHW, ontimeSH, ontimeDHW, auxonflagSH, auxonflagDHW, QcondSH, QevaSH, QcondDHW, QevaDHW, TcondSH, TevaSH, TcondDHW, TevaDHW, (EgroundSH .+ EgroundDHW), T_bh, Teva_in_hp];
end
"""
    groundheatpump(n::Int, fls::Array{T,1}, flsN::Array{T,1}, H::T, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4, Rb::T=0.15) where {T <:AbstractFloat}

Simulate the operation of a ground-source heat pump for space heating and/or domestic hot water during `n` years taking into account the presence of neighbouring installations

# Output:

1. `EcompressorSH` electricity used by the compressor in space heating mode [Wh]
2. `EauxSH` energy used by the auxiliary in space heating mode [Wh]
3. `EcompressorDHW` electricity used by the compressor in space heating mode [Wh]
4. `EauxDHW` electricity used by the auxiliary in domestic hot water mode [Wh]
5. `ontimeSH` share of the hour during which the HP operates in space heating mode [-]
6. `ontimeDHW` share of the hour during which the HP operates in domestic hot water mode [-]
7. `auxonflagSH` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in space heating mode [-]
8. `auxonflagDHW` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in domestic hot water mode [-]
9. `QcondSH` heating capacity in space heating mode [W]
10. `QevaSH` cooling capacity in space heating mode [W]
11. `QcondDHW` heating capacity in domestic hot water mode [W]
12. `QevaDHW` cooling capacity in domestic hot water mode [W]
13. `TcondSH` condensation temperature in space heating mode [K]
14. `TevaSH` evaporation temperature in space heating mode[K]
15. `TcondDHW` condensation temperature in domestic hot water mode [K]
16. `TevaDHW` evaporation temperature in domestic hot water mode [K]
17. `EgroundSH .+ EgroundDHW` heat absorbed from the ground [Wh]
18. `T_bh` borehole wall temperature [K]
19. `Teva_in_hp` return temperature from the ground heat exchnager [K]

# Arguments:

1. `n` number of years to simulate [-]
2. `fls` thermal response of the borehole to its own operation. Must already be scaled by 1/(2* pi *k) [m*K/W]
3. `flsN` thermal response of the borehole the operation of the neighbours. Must already be scaled by 1/(2* pi *k) [m*K/W]
4. `H` borehole length [m]
5. `TcondinSH` temperature of the return temperature from the space heating distribution system [K]
6. `loadSH` heat required for space heating [Wh]
7. `TcondinDHW` temperature of the return temperature from the domestic hot water production distribution system [K]
8. `loadDHW` heat required for hot water production [Wh]

## Keyword arguments:

- `Rb` borehole resistance[m*K/W]
- `parameters` heat pump thermodynamical parameters as in 'struct_HP_Data.jl' [-]
- `data` heat pump manufacturer data as in 'struct_HP_Parameters.jl' [-]
- `brine_flowrate_eva` mass flow rate of the secondary fluid in the borehole heat exchanger [kg/s]
- `water_flowrate_cond` mass flow rate of the water in the heating distribution system [kg/s]
"""
    function groundheatpump(n::Int, fls::Array{T,1}, flsN::Array{T,1}, H::T, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4, Rb::T = 0.15) where {T <:AbstractFloat}
    # @unpack Brine = data;
    Brine = "INCOMP::MEA-30%"
    ## Initializing variables
        T_bh = fill(0.0, 8760 * n);                                                             # Borehole wall temperature [K]
        T_bh[1] = 8. + 273.;                                                                    # The initial borehole temperature is 8 ºC
        Teva_in_hp = fill(0.0, 8760 * n);                                                       # Temperature of the fluid circulating in the borehole
        Teva_in_hp[1] = T_bh[1] - 3;                                                            # Initial return temperature of fluid circulating in the borehole 3 K lower than the borehole temperature. The best would be to find it iterating.

        Tcond_in_SH = repeat(TcondinSH, n);                                                     # Return temperature from the distribution system for space heating (water temperature at condenser inlet) [K]
        Tcond_in_DHW = repeat(TcondinDHW, n);                                                   # Return temperature from the distribution system for domestic hot water (water temperature at condenser inlet) [K]
        QloadSH = repeat(loadSH, n);                                                            # Heating demand for space heating [Wh]
        QloadDHW = repeat(loadDHW, n);                                                          # Heating demand for domestic hot water [Wh]

        QevaSH = fill(0.0, 8760 *n);                                                            # HP cooling capacity in space heating mode [W],
        TevaSH = fill(0.0, 8760 *n);                                                            # Refrigerant evaporation T in space heating mode [K]
        EevaSH = fill(0.0, 8760 *n);                                                            # Energy absorbed in the evaporator in space heating mode [Wh]
        EgroundSH = fill(0.0, 8760 *n);                                                         # Energy extracted from the BH in space heating mode [Wh]
        QevaDHW = fill(0.0, 8760 *n);                                                           # HP cooling capacity in domestic hot water mode [W],
        TevaDHW = fill(0.0, 8760 *n);                                                           # Refrigerant evaporation T in domestic hot water mode [K]
        EevaDHW = fill(0.0, 8760 *n);                                                           # Energy absorbed in the evaporator in domestic hot water mode [Wh]
        EgroundDHW = fill(0.0, 8760 *n);                                                        # Energy extracted from the BH in domestic hot water mode [Wh]

        QcondSH = fill(0.0, 8760 *n);                                                             # HP Heating capacity in space heating mode [W]
        TcondSH = fill(0.0, 8760 *n);                                                             # Refrigerant evaporation T in space heating mode [K]
        EcondSH = fill(0.0, 8760 *n);                                                             # Energy realeased in the condenser in space heating mode [Wh]
        QcondDHW = fill(0.0, 8760 *n);                                                             # HP Heating capacity in domestic hot water mode [W]
        TcondDHW = fill(0.0, 8760 *n);                                                             # Refrigerant evaporation T in domestic hot water mode [K]
        EcondDHW = fill(0.0, 8760 *n);                                                             # Energy realeased in the condenser in domestic hot water mode [Wh]

        EauxSH = fill(0.0, 8760 * n);                                                           # Energy for space heating delivered to the house by the auxiliary system [Wh]
        EauxDHW = fill(0.0, 8760 * n);                                                          # Energy for domestic hot water delivered to the house by the auxiliary system [Wh]

        WcompressorSH = fill(0.0, 8760 *n);                                                        # Compressor power in space heating mode [W]
        WcompressorDHW = fill(0.0, 8760 *n);                                                       # Compressor power in domestic hot water mode [W]
        EcompressorSH = fill(0.0, 8760 * n);                                                       # Energy used by the compressor for space heating [Wh]
        EcompressorDHW = fill(0.0, 8760 * n);                                                      # Energy used by the compressor for domestic hot water [Wh]

        ontimeSH = fill(0.0, 8760 *n);                                                             # Share of the hour while the HP is on for space heating [-]
        ontimeDHW = fill(0.0, 8760 *n);                                                            # Share of the hour while the HP is on for domestic hot water[-]

        auxonflagSH = fill(0.0, 8760 * n);                                                         # Flag indicating if the HP is used for spaced heating
        auxonflagDHW = fill(0.0, 8760 * n);                                                        # Flag indicating if the HP is used for domestic hot water

        AT_bh_extra = 0.;
        year = 0;
    ## end initialization
        for z = 1 : n * 8760
            if Teva_in_hp[z] < -12 + 273.15   # if the ground is too cold run the auxiliary system
                if z < n * 8760
                    if z % 8760 == 0
                        year = Int64(z/8760);
                        AT_bh_extra = sum((EgroundSH[1:z] .+ EgroundDHW[1:z])) / (z * H) * flsN[year];
                    end
                    AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls) + AT_bh_extra ; #for Q in W
                    T_bh[z+1] = T_bh[1] - AT_bh;
                    Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
                end
                auxonflagSH[z] = 2; # Auxiliary used for  SH
                auxonflagDHW[z] = 2; # Auxiliary used for DH
                EauxDHW[z] =  QloadDHW[z];
                EauxSH[z] = QloadSH[z];
            else # use the HP for DHW production
                HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_DHW[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
                TcondDHW[z], TevaDHW[z] = HPperformance[3], HPperformance[4];
                QcondDHW[z], QevaDHW[z] = HPperformance[1], HPperformance[2];
                WcompressorDHW[z] = QcondDHW[z] - QevaDHW[z];

                HPdelivered = hp_onoff(QcondDHW[z], QevaDHW[z], WcompressorDHW[z], QloadDHW[z], 1.);
                ontimeDHW[z] = HPdelivered[1];
                EevaDHW[z], EcompressorDHW[z], EauxDHW[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];
                EgroundDHW[z] = HPdelivered[2];
                EauxDHW[z] > 0.0 ? auxonflagDHW[z] = 1 : auxonflagDHW[z] = 0;
                EcondDHW[z] = QcondDHW[z] * ontimeDHW[z];
                if z % 8760 == 0
                    year = Int64(z/8760);
                    AT_bh_extra = sum((EgroundSH[1:z] .+ EgroundDHW[1:z])) / (z * H) * flsN[year];
                end
                AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls) + AT_bh_extra; #for Q in W
                T_bh[z] = T_bh[1] - AT_bh;
                Teva_in_hp[z] = T_bh[z] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);

            # SH production

                if Teva_in_hp[z] < -12 + 273.15   # if the ground is too cold run the auxiliary system
                    if z < n * 8760
                        T_bh[z+1] = T_bh[z];
                        Teva_in_hp[z+1] = Teva_in_hp[z];
                    end
                    auxonflagSH[z] = 2;   # Auxiliary used for SH
                    EauxSH[z] = QloadSH[z];
                else
                    HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
                    TcondSH[z], TevaSH[z] = HPperformance[3], HPperformance[4];
                    QcondSH[z], QevaSH[z] = HPperformance[1], HPperformance[2];
                    WcompressorSH[z] = QcondSH[z] - QevaSH[z];

                    HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1. - ontimeDHW[z]);

                    EgroundSH[z] = HPdelivered[2];
                    EevaSH[z] = HPdelivered[2];
                    EcompressorSH[z] = HPdelivered[3];
                    EauxSH[z] = HPdelivered[4];

                    EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;
                    EcondSH[z] = EcondSH[z] + QcondSH[z] * HPdelivered[1]
                    ontimeSH[z] = HPdelivered[1];
                    if z % 8760 == 0
                        year = Int64(z/8760);
                        AT_bh_extra = sum(EgroundSH[1:z] .+ EgroundDHW[1:z]) / (z * H) * flsN[year];
                    end
                    AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls) + AT_bh_extra; #for Q in W

                    if z < n*8760
                        T_bh[z+1] = T_bh[1] - AT_bh;
                        Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
                    end
                end
            end
        end
    return [EcompressorSH, EauxSH, EcompressorDHW, EauxDHW, ontimeSH, ontimeDHW, auxonflagSH, auxonflagDHW, QcondSH, QevaSH, QcondDHW, QevaDHW, TcondSH, TevaSH, TcondDHW, TevaDHW, (EgroundSH .+ EgroundDHW), T_bh, Teva_in_hp];
end

"""
    groundheatpump(n::Int, fls::Array{T,1}, H::T, TcondinSH::Array{T,1}, loadSH; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva::T = 0.3, water_flowrate_cond::T = 0.4, Rb::T = 0.15) where {T <:AbstractFloat}

Simulate the operation of a ground-source heat pump used for space heating and/or domestic hot water during `n` years neglecting the presence of neighbouring installations

# Output:

1. `EcompressorSH` electricity used by the compressor in space heating mode [Wh]
2. `EauxSH` energy used by the auxiliary in space heating mode [Wh]
3. `ontimeSH` share of the hour during which the HP operates in space heating mode [-]
4. `auxonflagSH` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in space heating mode [-]
5. `QcondSH` heating capacity in space heating mode [W]
6. `QevaSH` cooling capacity in space heating mode [W]
7. `TcondSH` condensation temperature in space heating mode [K]
8. `TevaSH` evaporation temperature in space heating mode [K]
9. `EgroundSH` heat absorbed from the ground [Wh]
10. `T_bh` borehole wall temperature [K]
11. `Teva_in_hp` return temperature from the ground heat exchnager [K]

# Arguments:

1. `n` number of years to simulate [-]
2. `fls` thermal response of the borehole to its own operation. Must already be scaled by 1/(2* pi *k) [m*K/W]
3. `H` borehole length [m]
4. `TcondinSH` temperature of the return temperature from the space heating distribution system [K]
5. `loadSH` heat required for space heating [Wh]

## Keyword arguments:

- `Rb` borehole resistance [m*K/W]
- `parameters` heat hump thermodynamical parameters as in 'struct_HP_Parameters.jl' [-]
- `data` heat hump manufacturer data as in 'struct_HP_Data.jl' [-]
- `brine_flowrate_eva` mass flow rate of the secondary fluid in the borehole heat exchanger [kg/s]
- `water_flowrate_cond ` mass flow rate of the water in the heating distribution system [kg/s]
"""
function groundheatpump(n::Int, fls::Array{T,1}, H::T, TcondinSH::Array{T,1}, loadSH; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4, Rb::T = 0.15) where {T <:AbstractFloat}
    @unpack Brine = data;

## Initializing the variables
    T_bh = fill(0.0, 8760 * n);                                                             # Borehole wall temperature [K]
    # This should be changed sothat the user can input their own value
    T_bh[1] = 8. + 273.;                                                                    # The initial borehole temperature is 8 ºC
    #
    Teva_in_hp = fill(0.0, 8760 * n);                                                       # Temperature of the fluid circulating in the borehole
    Teva_in_hp[1] = T_bh[1] - 3;                                                            # Initial return temperature of fluid circulating in the borehole 3 K lower than the borehole temperature. The best would be to find it iterating.

    Tcond_in_SH = repeat(TcondinSH, n);                                                     # Return temperature from the distribution system for space heating (water temperature at the condenser inlet) [K]                                               # Return temperature from the distribution system for domestic hot water (water temperature at the condenser inlet) [K]
    QloadSH = repeat(loadSH, n);                                                            # Heating demand for space heating [Wh]                                                         # Heating demand for domestic hot water [Wh]

    QevaSH = fill(0.0, 8760 *n);                                                            # HP cooling capacity in space heating mode [W],
    TevaSH = fill(0.0, 8760 *n);                                                            # Refrigerant evaporation T in space heating mode [K]
    EevaSH = fill(0.0, 8760 *n);                                                            # Energy absorbed in the evaporator in space heating mode [Wh]
    EgroundSH = fill(0.0, 8760 *n);                                                         # Energy extracted from the BH in space heating mode [Wh]

    QcondSH = fill(0.0, 8760 *n);                                                           # HP Heating capacity in space heating mode [W]
    TcondSH = fill(0.0, 8760 *n);                                                           # Refrigerant evaporation T in space heating mode [K]
    EcondSH = fill(0.0, 8760 *n);                                                           # Energy realeased in the condenser in space heating mode [Wh]

    EauxSH = fill(0.0, 8760 * n);                                                           # Energy for space heating delivered to the house by the auxiliary system [Wh]

    WcompressorSH = fill(0.0, 8760 *n);                                                     # Compressor power in space heating mode [W]
    EcompressorSH = fill(0.0, 8760 * n);                                                    # Energy used by the compressor for space heating [Wh]

    ontimeSH = fill(0.0, 8760 *n);                                                          # Share of the hour while the HP is on for space heating [-]

    auxonflagSH = fill(0.0, 8760 * n);                                                      # Flag indicating if the HP is used for spaced heating
## end initialization
    for z = 1 : n * 8760
        if Teva_in_hp[z] < -12 + 273.15   # if the ground is too cold run the auxiliary system. The threshold temperature should be initialized by the user
            if z < n * 8760
                AT_bh = self_dT((EgroundSH[1:z])./H, fls); # Temperature chang on the borehole wall [K]
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z]) / H * Rb + (EgroundSH[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
            auxonflagSH[z] = 2;           # Only the auxiliary is used
            EauxSH[z] = QloadSH[z];     # all the space heating demand is satisfied by the auxiliary
        else # use the HP
        # Space Heating
            HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
            TcondSH[z], TevaSH[z] = HPperformance[3], HPperformance[4];
            QcondSH[z], QevaSH[z] = HPperformance[1], HPperformance[2];
            WcompressorSH[z] = QcondSH[z] - QevaSH[z];
            HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1.);

            EgroundSH[z] = HPdelivered[2];
            EevaSH[z] = HPdelivered[2];
            EcompressorSH[z] = HPdelivered[3];
            EauxSH[z] = HPdelivered[4];

            EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;
            EcondSH[z] = QcondSH[z] * HPdelivered[1]
            ontimeSH[z] = HPdelivered[1];

            EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;

            if z < n * 8760
                AT_bh = self_dT((EgroundSH[1:z])./H, fls);
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] ) / H * Rb + (EgroundSH[z])  / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
        end
    end
return [EcompressorSH, EauxSH, ontimeSH, auxonflagSH, QcondSH, QevaSH, TcondSH, TevaSH, EgroundSH , T_bh, Teva_in_hp];
end
