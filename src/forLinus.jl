using HeatPump
using JLD
using XLSX

##  Input
H = 100;
##
path = string(dirname(pwd()), "\\HeatPump\\example\\files\\House_Output_tr")
Tcondin = load(path)["tr"];
path = string(dirname(pwd()), "\\HeatPump\\example\\files\\House_Output_Load")
loadhp = load(path)["hourload_No_DHW"];

Tcondin_DHW = fill(50. + 273.15, 8760);
path = string(dirname(pwd()), "\\HeatPump\\example\\files\\DHW_load.jld")
loadhp_DHW = load(path)["DHW_load"];

reference_house_load = sum(loadhp) + sum(loadhp_DHW);

avgCOP = 3.2;
avg_bh_load = 15 * H * 8760
avg_house_load = avg_bh_load * avgCOP / (avgCOP-1)
moltiplication_factor = avg_house_load / reference_house_load;

loadhp = loadhp .* moltiplication_factor;
loadhp_DHW  = loadhp_DHW  .* moltiplication_factor;
