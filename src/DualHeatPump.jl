"""
    dualheatpump(n::Int, fls::Array{T,1}, H::T, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4, Rb::T) where {T <:AbstractFloat}

Simulate the operation of a dual-source (air-ground) heat pump during `n` years neglecting the presence of neighbouring installations

# Output:

1. `EcompressorSH` Electricity used by the compressor in space heating mode [Wh]
2. `EauxSH` Energy used by the auxiliary in space heating mode [Wh]
3. `EcompressorDHW` Electricity used by the compressor in space heating mode [Wh]
4. `EauxDHW` Electricity used by the auxiliary in domestic hot water mode [Wh]
5. `ontimeSH` share of the hour during which the HP operates in space heating mode [-]
6. `ontimeDHW` share of the hour during which the HP operates domestic hot water [-]
7. `auxonflagSH` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in space heating mode [-]
8. `auxonflagDHW` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in domestic hot water mode [-]
9. `modeflag` set to 0 if the auxiliary is used, 1 if the ground loop is used, 2 if the air loop is used
10. `QcondSH` heating capacity in space heating mode [W]
11. `QevaSH` cooling capacity in space heating mode [W]
12. `QcondDHW` heating capacity domestic hot water [W]
13. `QevaDHW` cooling capacity  domestic hot water [W]
14. `TcondSH` condensation temperature in space heating mode [K]
15. `TevaSH` evaporation temperature in space heating mode[K]
16. `TcondDHW` condensation temperature domestic hot water [K]
17. `TevaDHW` evaporation temperature domestic hot water [K]
18. `EgroundSH .+ EgroundDHW` heat absorbed from the ground [Wh]
19. `T_bh` borehole wall temperature [K]
20. `Teva_in_hp` return temperature from the ground heat exchnager [K]

# Arguments:

1. `n` number of years to simulate [-]
2. `fls` thermal response of the borehole to its own operation. Should already be scaled by 1/(2* pi *k) [m*K/W]
3. `Tair` Outdoor air temperature [K]
4. `H` borehole length [m]
5. `TcondinSH` Temperature of the return temperature from the space heating distribution system [K]
6. `loadSH` Heat required for space heating [Wh]
7. `TcondinDHW` Temperature of the return temperature from the domestic hot water production distribution system [K]
8. `loadDHW` Heat required for hot water production [Wh]

## Keyword arguments:

- `Rb = 0.15` Borehole resistance[m*K/W]
- `parameters::HP_Parameters = HP_Parameters()` Heat Pump thermodynamical parameters as in "" [-]
- `data::HP_Data = HP_Data()` Heat Pump manufacturer data as in "" [-]
- `Teva_min = -12 + 273.15` minimum return brine temperature below which the HP is turned off and the auxiliary is used instead
- `brine_flowrate_eva = 0.3` Mass flow rate of the secondary fluid in the borehole heat exchanger [kg/s]
- `water_flowrate_cond = 0.4` Mass flow rate of the water in the heating distribution system [kg/s]
"""
function dualheatpump(n::Int, fls::Array{T,1}, Tair, H::T, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), Teva_min = -12 + 273.15, brine_flowrate_eva = 0.3, water_flowrate_cond = 0.4, Rb::T=0.15) where {T <:AbstractFloat}
    # @unpack Brine = data;
    Brine = "INCOMP::MEA-30%"
## Initializing variables
    T_bh = fill(0.0, 8760 * n);                                                             # Borehole wall temperature [K]
    T_bh[1] = 8. + 273.;                                                                    # The initial borehole temperature is 8 ºC
    Teva_in_hp = fill(0.0, 8760 * n);                                                       # Temperature of the fluid circulating in the borehole
    Teva_in_hp[1] = T_bh[1] - 3;                                                            # Initial return temperature of fluid circulating in the borehole 3 K lower than the borehole temperature. The best would be to find it iterating.

    Tair = repeat(Tair, n);

    Tcond_in_SH = repeat(TcondinSH, n);                                                     # Return temperature from the distribution system for space heating (water temperature at condenser inlet) [K]
    Tcond_in_DHW = repeat(TcondinDHW, n);                                                   # Return temperature from the distribution system for domestic hot water (water temperature at condenser inlet) [K]
    QloadSH = repeat(loadSH, n);                                                            # Heating demand for space heating [Wh]
    QloadDHW = repeat(loadDHW, n);                                                          # Heating demand for domestic hot water [Wh]

    QevaSH = fill(0.0, 8760 *n);                                                            # HP cooling capacity in space heating mode [W],
    TevaSH = fill(0.0, 8760 *n);                                                            # Refrigerant evaporation T in space heating mode [K]
    EevaSH = fill(0.0, 8760 *n);                                                            # Energy absorbed in the evaporator in space heating mode [Wh]
    EgroundSH = fill(0.0, 8760 *n);                                                         # Energy extracted from the BH in space heating mode [Wh]
    QevaDHW = fill(0.0, 8760 *n);                                                           # HP cooling capacity in domestic hot water mode [W],
    TevaDHW = fill(0.0, 8760 *n);                                                           # Refrigerant evaporation T in domestic hot water mode [K]
    EevaDHW = fill(0.0, 8760 *n);                                                           # Energy absorbed in the evaporator in domestic hot water mode [Wh]
    EgroundDHW = fill(0.0, 8760 *n);                                                        # Energy extracted from the BH in domestic hot water mode [Wh]

    Qcond_nc = fill(0.0, 8760 *n);

    QcondSH = fill(0.0, 8760 *n);                                                             # HP Heating capacity in space heating mode [W]
    TcondSH = fill(0.0, 8760 *n);                                                             # Refrigerant evaporation T in space heating mode [K]
    EcondSH = fill(0.0, 8760 *n);                                                             # Energy realeased in the condenser in space heating mode [Wh]
    QcondDHW = fill(0.0, 8760 *n);                                                             # HP Heating capacity in domestic hot water mode [W]
    TcondDHW = fill(0.0, 8760 *n);                                                             # Refrigerant evaporation T in domestic hot water mode [K]
    EcondDHW = fill(0.0, 8760 *n);                                                             # Energy realeased in the condenser in domestic hot water mode [Wh]

    EauxSH = fill(0.0, 8760 * n);                                                           # Energy for space heating delivered to the house by the auxiliary system [Wh]
    EauxDHW = fill(0.0, 8760 * n);                                                          # Energy for domestic hot water delivered to the house by the auxiliary system [Wh]

    WcompressorSH = fill(0.0, 8760 *n);                                                        # Compressor power in space heating mode [W]
    WcompressorDHW = fill(0.0, 8760 *n);                                                       # Compressor power in domestic hot water mode [W]
    EcompressorSH = fill(0.0, 8760 * n);                                                       # Energy used by the compressor for space heating [Wh]
    EcompressorDHW = fill(0.0, 8760 * n);                                                      # Energy used by the compressor for domestic hot water [Wh]

    ontimeSH = fill(0.0, 8760 *n);                                                             # Share of the hour while the HP is on for space heating [-]
    ontimeDHW = fill(0.0, 8760 *n);                                                            # Share of the hour while the HP is on for domestic hot water[-]

    auxonflagSH = fill(0.0, 8760 * n);                                                         # Flag indicating if the HP is used for spaced heating
    auxonflagDHW = fill(0.0, 8760 * n);                                                        # Flag indicating if the HP is used for domestic hot water

    modeflag = fill(0.0, 8760 * n);
## end initialization
    for z = 1 : n * 8760
        if Tair[z] -3 < Teva_min && Teva_in_hp[z] < Teva_min # if the air and ground is too cold run the auxiliary system
            AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls);
            if z < n * 8760
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
            modeflag[z] = 0;

            auxonflagSH[z] = 2;
            auxonflagDHW[z] = 2;

            EauxDHW[z] = QloadDHW[z];
            EauxSH[z] = QloadSH[z];
        elseif Tair[z] - 5 < Teva_in_hp[z] # if the air is colder than the ground use the ground
            # Domestic Hot water
                HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_DHW[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
                TcondDHW[z], TevaDHW[z] = HPperformance[3], HPperformance[4];
                QcondDHW[z], QevaDHW[z] = HPperformance[1], HPperformance[2];
                WcompressorDHW[z] = QcondDHW[z] - QevaDHW[z];

                HPdelivered = hp_onoff(QcondDHW[z], QevaDHW[z], WcompressorDHW[z], QloadDHW[z], 1.);
                ontimeDHW[z] = HPdelivered[1];
                EevaDHW[z], EcompressorDHW[z], EauxDHW[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];
                EgroundDHW[z] = HPdelivered[2];

                EauxDHW[z] > 0.0 ? auxonflagDHW[z] = 1 : auxonflagDHW[z] = 0;
                EcondDHW[z] = QcondDHW[z] * ontimeDHW[z];

                AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls);
                T_bh[z] = T_bh[1] - AT_bh;
                Teva_in_hp[z] = T_bh[z] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            # Space Heating
                if Teva_in_hp[z] < Teva_min   # if the ground is too cold run the auxiliary system
                    if z < n * 8760
                        T_bh[z+1] = T_bh[z];
                        Teva_in_hp[z+1] = Teva_in_hp[z];
                    end
                    auxonflagSH[z] = 2;   # All the space heating demand is satisfied by the auxiliary
                    EauxSH[z] = QloadSH[z];
                else
                    HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
                    TcondSH[z], TevaSH[z] = HPperformance[3], HPperformance[4];
                    QcondSH[z], QevaSH[z] = HPperformance[1], HPperformance[2];
                    WcompressorSH[z] = QcondSH[z] - QevaSH[z];

                    HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1. - ontimeDHW[z]);

                    EgroundSH[z] = HPdelivered[2];
                    EevaSH[z] = HPdelivered[2];
                    EcompressorSH[z] = HPdelivered[3];
                    EauxSH[z] = HPdelivered[4];

                    EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;
                    EcondSH[z] = QcondSH[z] * HPdelivered[1]
                    ontimeSH[z] = HPdelivered[1];

                    EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;

                    if z < n * 8760
                        AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls);
                        T_bh[z+1] = T_bh[1] - AT_bh;
                        Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] + EgroundDHW[z]) / H * Rb + (EgroundSH[z] + EgroundDHW[z])  / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
                    end
                end

            modeflag[z] = 1;    # the ground loop is used

        else # use the air
            HPperformance = hp_capacity(Tair[z] -3, Tcond_in_DHW[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
            TcondDHW[z], TevaDHW[z] = HPperformance[3], HPperformance[4];
            Qcond_nc[z], QevaDHW[z] = HPperformance[1], HPperformance[2];
            WcompressorDHW[z] = Qcond_nc[z] - QevaDHW[z];
            if Tair[z] < 7 + 273.15
                if Tair[z] >  273.15
                    F = min(1, 0.816 + 0.0177 * (Tair[z] - 273.15));
                else
                    F = 0.816;
                end
                QcondDHW[z] = F * Qcond_nc[z];
            else
                QcondDHW[z] = Qcond_nc[z]
            end

            HPdelivered = hp_onoff(QcondDHW[z], QevaDHW[z], WcompressorDHW[z], QloadDHW[z], 1.);
            ontimeDHW[z] = HPdelivered[1];
            EevaDHW[z], EcompressorDHW[z], EauxDHW[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];

            EauxDHW[z] > 0.0 ? auxonflagDHW[z] = 1 : auxonflagDHW[z] = 0;

            # SH
            HPperformance = hp_capacity(Tair[z] -3, Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
            TcondSH[z], TevaSH[z] = HPperformance[3], HPperformance[4];
            Qcond_nc[z], QevaSH[z] = HPperformance[1], HPperformance[2];
            WcompressorSH[z] = Qcond_nc[z] - QevaSH[z];
            if Tair[z] < 7 + 273.15
                if Tair[z] >  273.15
                    F = min(1, 0.816 + 0.0177 * (Tair[z] - 273.15));
                else
                    F = 0.816;
                end
                QcondSH[z] = F * Qcond_nc[z];
            else
                QcondSH[z] = Qcond_nc[z]
            end

            HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1. - ontimeDHW[z]);
            ontimeSH[z] = HPdelivered[1];
            EevaSH[z], EcompressorSH[z], EauxSH[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];

            EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;

            if z < 8760 * n
                AT_bh = self_dT((EgroundSH[1:z] .+ EgroundDHW[1:z])./H, fls); #for Q in W
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - (EgroundSH[z] .+ EgroundDHW[z]) / H * Rb + (EgroundSH[z] .+ EgroundDHW[z]) / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
            modeflag[z] = 2; # the air is used
        end
    end
    return [EcompressorSH, EauxSH, EcompressorDHW, EauxDHW, ontimeSH, ontimeDHW, auxonflagSH, auxonflagDHW, modeflag, QcondSH, QevaSH, QcondDHW, QevaDHW, TcondSH, TevaSH, TcondDHW, TevaDHW, (EgroundSH .+ EgroundDHW), T_bh, Teva_in_hp];
end

function dualheatpump(n::Int, fls::Array{T,1}, flsN::Array{T,1}, Tair, H::T, TcondinSH::Array{T,1}, loadSH, TcondinDHW::Array{T,1}, loadDHW; parameters::HP_Parameters = HP_Parameters(), data::HP_Data = HP_Data(), brine_flowrate_eva::T = 0.3, water_flowrate_cond::T = 0.4, Teva_min = -12 + 273.15, Rb::T = 0.15) where {T <:AbstractFloat}
    # @unpack Brine = data;
    Brine = "INCOMP::MEA-30%"
    Tair = repeat(Tair, n);
    T_bh = fill(0.0, 8760 * n);                                                             # Temperature at the borehole wall [K]
    T_bh[1] = 8. + 273.;                                                                    # The initial borehole temperature is 8 ºC
    Teva_in_hp = fill(0.0, 8760 * n);                                                       # Temperature of the fluid circulating in the borehole
    Teva_in_hp[1] = T_bh[1] - 3;                                                            # Initial return temperature of fluid circulating in the borehole 3 K lower than the borehole temperature. The best would be to find it iterating.

    Tcond_in_SH = repeat(TcondinSH, n);                                                     # Return temperature from the distribution system of the house (water temperature at condenser inlet) [K]
    Tcond_in_DHW = repeat(TcondinDHW, n);                                                   # Return temperature from the distribution system of the house (water temperature at condenser inlet) [K]

    QloadSH = repeat(loadSH, n);                                                            # Heat load of the house [W]
    QloadDHW = repeat(loadDHW, n);                                                                    # Heat load of the house [W]

    Qeva = fill(0.0, 8760 *n);                                                              # HP cooling capacity [W],
    Teva = fill(0.0, 8760 *n);                                                              # Condensation T [T]
    Eeva = fill(0.0, 8760 *n);                                                              # Energy extracted from the BH [Wh]
    Eground = fill(0.0, 8760 *n);
    Qcond = fill(0.0, 8760 *n);                                                             # HP Heating capacity [W], ,
    Qcond_nc = fill(0.0, 8760 *n);
    Tcond = fill(0.0, 8760 *n);                                                             # Evaporation T[K]
    EauxDHW = fill(0.0, 8760 * n);                                                          # Energy delivered to the house by the auxiliary system [Wh]
    EauxSH = fill(0.0, 8760 * n);                                                           # Energy delivered to the house by the auxiliary system [Wh]
    Wcompressor = fill(0.0, 8760 *n);                                                       # Compressor power [W]
    EcompressorSH = fill(0.0, 8760 *n);                                                       # Compressor power [W]
    EcompressorDHW = fill(0.0, 8760 * n);                                                   # Compressor electricity use [Wh]
    COP = fill(0.0, 8760 * n);                                                              # COP [-]
    ontime = fill(0.0, 8760 *n);                                                            # Share of the hour while the HP is on
    auxonflag = fill(0.0, 8760 * n);                                                        # Flag indicating if the HP is used or not
    modeflag = fill(0.0, 8760 * n);

    AT_bh_extra = 0.;
    y = 0;
    for z = 1 : n * 8760
        if Tair[z] -3 < 273.15 - 12 && Teva_in_hp[z] < 273.15 - 12 # if the air and ground is too cold run the auxiliary system
            if z < n*8760
                if z % 8760 == 0
                    y = Int64(z/8760);
                    AT_bh_extra = sum(Eground[1:z]) / (z * H) * flsN[y];
                end
                AT_bh = self_dT(Eground[1:z]./H, fls) + AT_bh_extra; #for Q in W
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - Eground[z] / H * Rb + Eground[z] / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
            modeflag[z] = 2;
            EauxDHW[z] = QloadDHW[z];
            EauxSH[z] = QloadSH[z];
        elseif Tair[z] - 5 < Teva_in_hp[z] # if the air is colder than the ground use the ground

            HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_DHW[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);

            Tcond[z], Teva[z] = HPperformance[3], HPperformance[4];
            Qcond[z], Qeva[z] = HPperformance[1], HPperformance[2];
            Wcompressor[z] = Qcond[z] - Qeva[z];

            HPdelivered = hp_onoff(Qcond[z], Qeva[z], Wcompressor[z], QloadDHW[z], 1.);
            ontime[z] = HPdelivered[1];
            Eeva[z], EcompressorDHW[z], EauxDHW[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];
            Eground[z] = HPdelivered[2];
            EauxDHW[z] > 0.0 ? auxonflag[z] = 1 : auxonflag[z] = 0;
            if z % 8760 == 0
                y = Int64(z/8760)
                AT_bh_extra = sum(Eground[1:z]) / (z * H) * flsN[y];
            end
            AT_bh = self_dT(Eground[1:z]./H, fls) + AT_bh_extra; #for Q in W
            T_bh[z] = T_bh[1] - AT_bh;
            Teva_in_hp[z] = T_bh[z] - Eground[z] / H * Rb + Eground[z] / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);

            modeflag[z] = 1;    # the ground loop is used

            HPperformance = hp_capacity(Teva_in_hp[z], Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
            Tcond[z], Teva[z] = HPperformance[3], HPperformance[4];
            Qcond[z], Qeva[z] = HPperformance[1], HPperformance[2];
            Wcompressor[z] = Qcond[z] - Qeva[z];

            HPdelivered = hp_onoff(Qcond[z], Qeva[z], Wcompressor[z], QloadSH[z], 1. - ontime[z]);

            Eeva[z], EcompressorSH[z], EauxSH[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];
            Eground[z] =  Eground[z] + HPdelivered[2];
            EauxSH[z] > 0.0 ? auxonflag[z] = 1 : auxonflag[z] = 0;

            ontime[z] = ontime[z] + HPdelivered[1];
            if z < 8760 * n
                if z % 8760 == 0
                    y = Int64(z/8760)
                    AT_bh_extra = sum(Eground[1:z]) / (z * H) * flsN[y];
                end
                AT_bh = self_dT(Eground[1:z]./H, fls) + AT_bh_extra ; #for Q in W
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - Eground[z] / H * Rb + Eground[z] / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
            modeflag[z] = 1;    # the ground loop is used

        else # use the air
            HPperformance = hp_capacity(Tair[z] -3, Tcond_in_DHW[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
            Tcond[z], Teva[z] = HPperformance[3], HPperformance[4];
            Qcond_nc[z], Qeva[z] = HPperformance[1], HPperformance[2];
            Wcompressor[z] = Qcond_nc[z] - Qeva[z];
            if Tair[z] < 7 + 273.15
                if Tair[z] >  273.15
                    F = min(1, 0.816 + 0.0177 * (Tair[z] - 273.15));
                else
                    F = 0.816;
                end
                Qcond[z] = F * Qcond_nc[z];
            else
                Qcond[z] = Qcond_nc[z]
            end

            HPdelivered = hp_onoff(Qcond[z], Qeva[z], Wcompressor[z], QloadDHW[z], 1.);
            ontime[z] = HPdelivered[1];
            Eeva[z], EcompressorDHW[z], EauxDHW[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];

            EauxDHW[z] > 0.0 ? auxonflag[z] = 1 : auxonflag[z] = 0;

            # SH
            HPperformance = hp_capacity(Tair[z] -3, Tcond_in_SH[z], brine_flowrate_eva, water_flowrate_cond, parameters, data);
            Tcond[z], Teva[z] = HPperformance[3], HPperformance[4];
            Qcond_nc[z], Qeva[z] = HPperformance[1], HPperformance[2];
            Wcompressor[z] = Qcond_nc[z] - Qeva[z];
            if Tair[z] < 7 + 273.15
                if Tair[z] >  273.15
                    F = min(1, 0.816 + 0.0177 * (Tair[z] - 273.15));
                else
                    F = 0.816;
                end
                Qcond[z] = F * Qcond_nc[z];
            else
                Qcond[z] = Qcond_nc[z]
            end

            HPdelivered = hp_onoff(Qcond[z], Qeva[z], Wcompressor[z], QloadSH[z], 1.);
            ontime[z] = HPdelivered[1];
            Eeva[z], EcompressorSH[z], EauxSH[z] = HPdelivered[2], HPdelivered[3], HPdelivered[4];

            EauxSH[z] > 0.0 ? auxonflag[z] = 1 : auxonflag[z] = 0;

            if z < 8760 * n
                if z % 8760 == 0
                    y = Int64(z/8760);
                    AT_bh_extra = sum(Eground[1:z]) / (z * H) * flsN[y];
                end
                AT_bh = self_dT(Eground[1:z]./H, fls) + AT_bh_extra ; #for Q in W
                T_bh[z+1] = T_bh[1] - AT_bh;
                Teva_in_hp[z+1] = T_bh[z+1] - Eground[z] / H * Rb + Eground[z] / 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", Teva_in_hp[z], Brine);
            end
        end
    end
    return [Qcond, Qeva, Wcompressor, Tcond, Teva, ontime, Eeva, EcompressorSH, EauxSH, auxonflag, Teva_in_hp, Eground, T_bh, modeflag, EcompressorDHW, EauxDHW];
end
