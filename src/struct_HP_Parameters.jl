# Optimized parameters obtained using the function "optmizedparameters".
# minimum = 2.716190e-03
using Parameters

@with_kw struct HP_Parameters
#Optimized parameters for the ETON 6 kW HP. Associated to the manufacturer data in "struct_HP_Data.jl"
    Refrigerant = "R410A";

# Data obtained using "INCOMP::MEA-25%" as a brine
    Brine = "INCOMP::MEA-25%"

    Vr = 2.314544210978224
    V = 0.0011860195588884995
    C = 4.2357264067141015e-6
    ΔTsuperheating = 0.10000000000001094
    compressor_efficiency = 0.7692327866797489
    W_loss = 8.960951699908507e-14
    UAeva = 1151.2723808074418
    UAcond = 6734.800379655178

# Data obtained using water as brine (as in the catalogue)
    # Brine = "water";
    #
    # Vr = 2.3145498670555926
    # V = 0.0011861155932471345
    # C = 4.5651723157773725e-6
    # ΔTsuperheating = 0.10000000000354682
    # compressor_efficiency = 0.7692942282596087
    # W_loss = 2.9047338970573206e-11
    # UAeva = 1150.8753827898165
    # UAcond = 6733.872444140246

#Optimized parameters for the NIBE 8kW HP. Associated to the manufacturer data in "struct_HP_Data.jl"
    # Brine = "INCOMP::MEA-25%"; # 25% ethanol
    # Refrigerant = "R407C";
    #
    # Vr = 5.028643057295242 ;                       # built-in volume ratio [-]
    # V = 0.002140541735513396;                     # nominal refrigerant volume flow rate [m3/s]
    # C = 0.0008029531489722598;                       # leakage coefficient of the compressor [kg/s]
    # ΔTsuperheating = 0.10000000000000338;            # degree of superheating [K]
    # compressor_efficiency = 0.9899999999999999;     # electro-mechanical efficiency of the compressor
    # W_loss = 4.27123144251456e-14;                # constant part of the compressor power losses [W]
    # UAeva = 54427.59608207937;                     # heat transfer coefficient of the evaporator [W/K]
    # UAcond = 37263.28639266395;                    # heat transfer coefficient of the condenser [W/K]
    #

    # Vr = 1.752250830330701
    # V = 0.0011738423043124858
    # C = 3.796093348815745e-6
    # ΔTsuperheating =1.0000000000012157
    # compressor_efficiency = 0.9799999999999985
    # W_loss = 6.344419449913073e-12
    # UAeva = 12996.568122802866
    # UAcond = 391.6569373278332

end
